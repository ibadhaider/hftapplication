import { Injectable } from '@angular/core';
import { Trader } from '../model/trader';

@Injectable({
  providedIn: 'root'
})

export class ThemeOptions {
  globalTraders = [];
  sidebarHover = false;
  toggleSidebar = false;
  toggleSidebarMobile = false;
  toggleHeaderMobile = false;
  toggleFixedFooter = false;
}
