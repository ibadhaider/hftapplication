import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class JsonDataService {

  localUrl = "assets/BasePriceJson.json";
  
  constructor(private http:HttpClient) { }

  public getDataJSON() {
    let c = this.http.get(this.localUrl)
    console.log(c)
    return c
  }
}
