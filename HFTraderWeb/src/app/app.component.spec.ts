import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { BaseComponent } from 'src/view/base-layout/base-component';
import { TraderHistoryComponent } from 'src/view/trader-history/trader-history.component';
import { HeaderComponent } from 'src/view/header/header.component';
import { SidebarComponent } from 'src/view/sidebar/sidebar.component';
import { TraderToolbar } from 'src/view/trader-toolbar/trader-toolbar';
import { ProfileCharts } from 'src/view/profile-charts/profile-charts';
import { TraderTable2MA } from 'src/view/trader-table/trader-table-2ma/trader-table-2ma';
import { TraderTableBB } from 'src/view/trader-table/trader-table-bb/trader-table-bb';
import { TraderTableAll } from 'src/view/trader-table/trader-table-all/trader-table-all';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSortModule } from '@angular/material/sort';
import { HttpClientModule } from '@angular/common/http';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        FormsModule,
        CommonModule,
    
        // Angular Bootstrap Components
        NgbModule,
        MatSortModule,
        HttpClientModule
      ],
      declarations: [
        AppComponent,
        BaseComponent,
        HeaderComponent,
        SidebarComponent,
        TraderToolbar,
    
        ProfileCharts,
      
        //Table Classes
        TraderTable2MA,
        TraderTableBB,
        TraderTableAll,
        TraderHistoryComponent 
        
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'Automated Trading Platform'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Automated Trading Platform');
  });


});
