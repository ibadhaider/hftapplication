//Core Imports
import { BrowserModule  } from '@angular/platform-browser';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule} from '@angular/router'; //NOT YET
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http'; 

//Bootstrap Imports
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSortModule} from '@angular/material/sort';

//Layout
import { BaseComponent } from '../view/base-layout/base-component'
import { HeaderComponent} from '../view/header/header.component';
import { SidebarComponent } from '../view/sidebar/sidebar.component';
import { TraderToolbar } from '../view/trader-toolbar/trader-toolbar';

import { ProfileCharts } from 'src/view/profile-charts/profile-charts';

//Table Class Imports
import { TraderTable2MA } from 'src/view/trader-table/trader-table-2ma/trader-table-2ma';
import { TraderTableBB } from 'src/view/trader-table/trader-table-bb/trader-table-bb';
import { TraderTableAll } from 'src/view/trader-table/trader-table-all/trader-table-all';
import { TraderHistoryComponent } from '../view/trader-history/trader-history.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    CommonModule,

    // Angular Bootstrap Components
    NgbModule,
    MatSortModule,
    HttpClientModule
  ],

  declarations: [
    //Layout
    AppComponent,
    BaseComponent,
    HeaderComponent,
    SidebarComponent,
    TraderToolbar,

    ProfileCharts,
  
    //Table Classes
    TraderTable2MA,
    TraderTableBB,
    TraderTableAll,
    TraderHistoryComponent 
  ],

  //REQUIRE FOR MODAL 
  entryComponents: [
    TraderHistoryComponent, TraderToolbar
  ],
  providers: [],
  exports: [TraderToolbar],
  bootstrap: [AppComponent]

})
export class AppModule { }
