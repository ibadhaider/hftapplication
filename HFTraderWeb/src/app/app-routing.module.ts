import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BaseComponent } from 'src/view/base-layout/base-component';
import { HeaderComponent } from 'src/view/header/header.component';

import { TraderTable2MA } from 'src/view/trader-table/trader-table-2ma/trader-table-2ma';
import { TraderTableBB } from 'src/view/trader-table/trader-table-bb/trader-table-bb';
import { TraderTableAll } from 'src/view/trader-table/trader-table-all/trader-table-all';
import { ProfileCharts } from 'src/view/profile-charts/profile-charts';

const routes: Routes = [
  {
    path: '',
    component: TraderTableAll
  },
  {
    path: 'All',
    component: TraderTableAll
  },
  {
    path: '2MA',
    component: TraderTable2MA
  },
  {
    path: 'BB',
    component: TraderTableBB
  },

  {
    path: "profile",
    component: ProfileCharts
  },
  
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
      scrollPositionRestoration: 'enabled',
      anchorScrolling: 'enabled',
    })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
