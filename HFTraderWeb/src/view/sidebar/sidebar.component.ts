import {Component} from '@angular/core';
import {Observable} from 'rxjs';
import {ThemeOptions} from '../../app/theme-options';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent {

  showPortfolio=false;
  showStrategies = true;
  constructor() {
  }
}
