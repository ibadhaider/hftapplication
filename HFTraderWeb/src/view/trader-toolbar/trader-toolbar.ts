import { Component, ViewChild } from "@angular/core";
import { Trader } from "../../model/trader";
import { TwoMovingAverages } from "../../model/two-moving-averages";
import { TraderService } from "../../model/trader-service";
import { BollingerBands } from 'src/model/bollinger-bands';

import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import {JsonDataService} from "../../app/json-data.service"
import { NgbTypeahead, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

/**
 * Angular component for a toolbar that allows the user to configure
 * and to create new traders. The component logic and the HTML template
 * currently support only the 2MA trader type.
 *
 * @author Will Provost
 */
@Component({
  selector: "trader-toolbar",
  templateUrl: "./trader-toolbar.html",
  styleUrls: ["./trader-toolbar.css"],
  }) 

export class TraderToolbar {

  public model: any;
  type: string;
  stock: string;
  size: number;

  // 2MA
  lengthShort: number;
  lengthLong: number;
  
  // BB
  length: number;
  multiple: number;

  exitThreshold: number;

  //TypeAhead
  jsondataOb1: Array<string>;
  jsondataOb;

  
  @ViewChild('typeaheadInstance', {static: false})
  private typeaheadInstance: NgbTypeahead;

  formatter = (result: string) => result.toUpperCase();



  /**
   * Set default values for all properties, which will flow out to the
   * initial UI via two-way binding.
   */
  constructor(private service: TraderService,  private readData: JsonDataService, private activeModal: NgbActiveModal) {
    this.service = service;

    this.type = "2MA";
    this.stock = "MRK";
    this.size = 1000;
    this.lengthShort = 30;
    this.lengthLong = 60;
    this.exitThreshold = 3;

    this.length = 60;
    this.multiple = 2;
  }

  ngOnInit(){
    this.readData.getDataJSON().subscribe((data) => {
         this.jsondataOb = data;   
      // console.log(this.jsondataOb);
      // console.log(this.jsondataOb.map(stock => stock.Stock)) 
          this.jsondataOb1 =  this.jsondataOb.map(stock => stock.Stock) 
    });
   }


  /**
   * Reads the values of form controls via two-way binding.
   * Creates an instance of the trader (only 2MA traders currently supported)
   * and sends it to the server to be created and activated.
   */
  create() {
    switch (this.type){
      case "2MA":
        this.service.createTrader(new TwoMovingAverages
          (0, this.stock, this.size, true, false, [], 0, NaN,
            this.lengthShort * 1000, this.lengthLong * 1000, this.exitThreshold / 100));
        break;
      case "BB":
        console.log("New  BB");
        this.service.createTrader(new BollingerBands
          (0, this.stock, this.size, true, false, [], 0, NaN,
            this.length * 1000, this.multiple, this.exitThreshold / 100));
          break;
    }

  }
  
  search = (text$: Observable<string>) =>
  text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    
    map(term => term === '' ? []
      : this.jsondataOb1.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1))
  )
  
  typeaheadKeydown($event: KeyboardEvent) {
    if (this.typeaheadInstance.isPopupOpen()) {
      setTimeout(() => {
        const popup = document.getElementById(this.typeaheadInstance.popupId);
        const activeElements = popup.getElementsByClassName('active');
        if (activeElements.length === 1) {
          // activeElements[0].scrollIntoView();
          const elem = (activeElements[0] as any);
          if (typeof elem.scrollIntoViewIfNeeded === 'function') {
            // non standard function, but works (in chrome)...
            elem.scrollIntoViewIfNeeded();
          } else {
            //do custom scroll calculation or use jQuery Plugin or ...
            this.scrollIntoViewIfNeededPolyfill(elem as HTMLElement);
          }
        }
      });
    }
  }

  /**
   * ... use https://gist.github.com/hsablonniere/2581101
   */
  private scrollIntoViewIfNeededPolyfill(elem: HTMLElement, centerIfNeeded = true) {
    var parent = elem.parentElement,
        parentComputedStyle = window.getComputedStyle(parent, null),
        parentBorderTopWidth = parseInt(parentComputedStyle.getPropertyValue('border-top-width')),
        parentBorderLeftWidth = parseInt(parentComputedStyle.getPropertyValue('border-left-width')),
        overTop = elem.offsetTop - parent.offsetTop < parent.scrollTop,
        overBottom = (elem.offsetTop - parent.offsetTop + elem.clientHeight - parentBorderTopWidth) > (parent.scrollTop + parent.clientHeight),
        overLeft = elem.offsetLeft - parent.offsetLeft < parent.scrollLeft,
        overRight = (elem.offsetLeft - parent.offsetLeft + elem.clientWidth - parentBorderLeftWidth) > (parent.scrollLeft + parent.clientWidth),
        alignWithTop = overTop && !overBottom;

    if ((overTop || overBottom) && centerIfNeeded) {
      parent.scrollTop = elem.offsetTop - parent.offsetTop - parent.clientHeight / 2 - parentBorderTopWidth + elem.clientHeight / 2;
    }

    if ((overLeft || overRight) && centerIfNeeded) {
      parent.scrollLeft = elem.offsetLeft - parent.offsetLeft - parent.clientWidth / 2 - parentBorderLeftWidth + elem.clientWidth / 2;
    }

    if ((overTop || overBottom || overLeft || overRight) && !centerIfNeeded) {
      elem.scrollIntoView(alignWithTop);
    }
  };

}
