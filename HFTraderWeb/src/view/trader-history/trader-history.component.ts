import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-trader-history',
  templateUrl: './trader-history.component.html',
  styleUrls: ['./trader-history.component.css']
})
export class TraderHistoryComponent implements OnInit {
  
  @Input() positions: Array<Position>;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

}
