//Module Imports
import { Component, OnInit} from "@angular/core";
import * as CanvasJS from '../../canvasjs.min';

//Class Imports
import { Trader } from "../../model/trader";
import { TraderService } from "../../model/trader-service";
import { TraderUpdate } from "../../model/trader-service";

/**
 * Angular component showing all current traders, including their
 * type, parameters, state, and profitability.
 *
 * @author Will Provost
 */
@Component({
  selector: "profile-charts",
  templateUrl: "./profile-charts.html",
  styleUrls: ["./profile-charts.css"]
})


 
export class ProfileCharts implements OnInit, TraderUpdate {

	 //Class Variables
	 service: TraderService;
	 traders: Array<Trader> = [];
	 data = [];
	 updates=0;
	 type:string = "all";
	 profitPoints = [];
	 tradePoints=[{y : 0, label: "2MA" },{y : 0, label: "BB" }];

  /**
   * Store the injected service references and modal service.
   */
  constructor(service: TraderService){
    this.service = service;
    service.subscribe(this);
	service.notify();
  }
	ngOnInit() {
		this.updateTradePoints(this.tradePoints);

	let chartTrades = new CanvasJS.Chart("chartContainer", {
		animationEnabled: true,
		exportEnabled: true,
		title: {
			text: "Trades Per Strategy"
		},
		data: [{
			type: "column",
			dataPoints: this.tradePoints
		}],
	});

	//chart for point 2
	let chartProfit = new CanvasJS.Chart("chartContainer2", {
		animationEnabled: true,
		exportEnabled: true,
		title: {
			text: "Total Profit"
		},
		data: [{
			type: "spline",
			dataPoints: this.profitPoints
		}],
	});

	//updates total trades map value then pushes to points{}
	this.updateTradePoints(this.tradePoints);
	//this.updateProfitPoints(this.profitPoints);

	//recursive function
	updateChart(this, this.tradePoints, this.profitPoints);
		
	function updateChart(p: ProfileCharts,tradePoints: any[],profitPoints) {	
		p.updateTradePoints(tradePoints);
		p.updateProfitPoints(profitPoints);
			chartTrades.render();
			chartProfit.render();
			setTimeout(function(){updateChart(p,tradePoints,profitPoints)}, 3000);	
		}
	}

	/**
   * Replace our array of traders with the latest,
   * which will triger a UI update.
   */
  latestTraders(traders: Array<Trader>) {
	this.traders = traders;
	console.log("Profile got new Traders");
  }

  updateTradePoints(tradePoints: any[]){
	this.getTotalTrades();
	console.log("Totaltrades ");
		tradePoints.pop();
		tradePoints.pop();
	  	tradePoints.push({y :this.data['2MAt'], label:"2MA"});
	  	tradePoints.push({y :this.data['BBt'], label:"BB"});
  }
  
  updateProfitPoints(points: any[]){
	this.getTotalProfit();
	this.profitPoints.push({y : this.data['profit'], x: this.updates});

	if(this.profitPoints.length>9){
		//this.profitPoints=this.profitPoints.slice(2,);
	}

	this.updates=this.updates+1;
  }

  getFilteredTraders(type: string){
	return this.traders.filter(t => {
	  return t["@type"] == type
	 });
  }//Done

 	getTotalTrades() {
	 this.data['2MAt']= this.getFilteredTraders('2MA').map(t => t.trades).reduce((x, y) => x + y, 0);  	  
	 this.data['BBt']= this.getFilteredTraders('BB').map(t => t.trades).reduce((x, y) => x + y, 0);
	}//Done

	getTotalProfit() {
		this.data['profit']= this.traders.map(t => t.profitOrLoss).reduce((x, y) => x + y, 0)
	}//Done

  updateValues(){
	this.getTotalTrades();
	this.getTotalProfit();
  }
}
