//Module Imports
import { Component, Output, Input } from "@angular/core";
import { Sort } from '@angular/material/sort';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

//Class Imports
import { Trader } from "../../../model/trader";
import { TwoMovingAverages } from "../../../model/two-moving-averages";
import { TraderService } from "../../../model/trader-service";
import { TraderUpdate } from "../../../model/trader-service";
import { Trade } from 'src/model/trade';
import { Position } from '../../../model/position';
import { TraderHistoryComponent } from '../../trader-history/trader-history.component';
import { ThemeOptions } from 'src/app/theme-options';
import { TraderToolbar } from 'src/view/trader-toolbar/trader-toolbar';

/**
 * Angular component showing all current traders, including their
 * type, parameters, state, and profitability.
 *
 * @author Will Provost
 */
@Component({
  selector: "trader-table-2ma",
  templateUrl: "./trader-table-2ma.html",
  styleUrls: ["./trader-table-2ma.css"]
})
export class TraderTable2MA implements TraderUpdate{
  ///Class Variables
  service: TraderService;
  traders: Array<Trader> = [];
  currentPositions: Array<Position> = [];

  type:string = "2MA";
  /**
   * Store the injected service references and modal service.
   */
  constructor(service: TraderService, public globals: ThemeOptions, private modalService: NgbModal){
    this.service = service;
    service.subscribe(this);
    service.notify();
  }

  handleCreate(){
    this.modalService.open(TraderToolbar, {size:"xl", windowClass: 'large-Modal'});
  }

   /**
   * Replace our array of traders with the latest,
   * which will triger a UI update.
   */
  latestTraders(traders: Array<Trader>) {
    this.traders = traders;
  }

/**
   Helper to format times in mm:ss format.
   */
  minSec(millis: number): string {
    let seconds = millis / 1000;
    const minutes = Math.floor(seconds / 60);
    seconds = seconds % 60;
    const pad = seconds < 10 ? "0" : "";
    return "" + minutes + ":" + pad + seconds;
  }
  /**
   * Helper to derive a label for the trader's state: "Started",
   * "Stopped", or, if deactivated but still closing out a position,
   * "Stopping".
   */
  getState(trader: Trader) {
    if(trader.stopping){
      return "Stopping"

    }
    else if(trader.active){
      return "Started"
    }
    else if (!trader.active){
      return "Stopped"
    }

  }

  /**
   * Helper to derive the total number of trades made by this trader.
   */
  getTotalTrades(): number {
    return this.traders.map(t => t.trades).reduce((x, y) => x + y, 0);
  }

  /**
   * Helper to derive the trader's total profit.
   */
  getTotalProfit(): number {
    return this.traders.map(t => t.profitOrLoss).reduce((x, y) => x + y, 0);
  }

  /**
   * Used to sort tables by attribute
   * @param sort
   */
  sortData(sort: Sort) {
    const data = this.traders.slice();
    if (!sort.active || sort.direction === '') {
      this.traders = data;
      return;
    }

    this.traders = data.sort((a : any , b : any ) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'stock': return this.compare(a.stock, b.stock, isAsc);
        case 'size': return this.compare(a.size, b.size, isAsc);
        case 'exit': return this.compare(a.exitThreshold, b.exitThreshold, isAsc);
        case 'trades': return this.compare(a.trades, b.trades, isAsc);
        case 'profit': return this.compare(a.profitOrLoss, b.profitOrLoss, isAsc);
        case 'roi': return this.compare(a.ROI, b.ROI, isAsc);
        default: return 0;
      }
    });
  }

  /**
   * Helper to sort tables
  **/
  compare(a: number | string, b: number | string, isAsc: boolean) {
      return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  /**
   * Finds the trader at the given table index and uses the
   * TraderService component to send an HTTP request to toggle the
   * trader's state.
   */
  startOrStop(trader: Trader, hardStop: boolean, type: string) {
    
    this.service.setActive(trader.ID, !trader.active);
  }

  handleHistory(trader : Trader): void {
    this.currentPositions = trader.positions;
    let modalRef = this.modalService.open(TraderHistoryComponent, {size:"xl", scrollable: true});
    modalRef.componentInstance.positions=this.currentPositions;

  }

}
