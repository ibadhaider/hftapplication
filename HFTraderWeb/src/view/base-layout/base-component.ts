//Module Imports
import { Component, Output, OnInit } from "@angular/core";


//Class Imports
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


/**
 * Angular component keeps track of all traders, positons, and trades
 *
 * @author Will Provost
 */
@Component({
  selector: 'app-base-component',
  templateUrl: './base-component.html',
  styleUrls: ['./base-component.css']
})

export class BaseComponent {
  /**
   * Store the injected service references and modal service.
   */
  show =true
  constructor(private modalService: NgbModal){

  }

  
  
}