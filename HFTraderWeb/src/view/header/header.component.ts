import {Component, Input} from '@angular/core';
import {Observable} from 'rxjs';
import {ThemeOptions} from '../../app/theme-options';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  
  title = "Automated Trading Platform"; 

  constructor(public globals: ThemeOptions) {
  }
}
