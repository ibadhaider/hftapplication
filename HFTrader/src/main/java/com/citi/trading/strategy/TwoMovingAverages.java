package com.citi.trading.strategy;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


/**
 * Represents a two-moving-averages trading strategy, adding the parameters
 * that are specific to that algorithm.
 * 
 * @author Will Provost
 */
@Entity
@DiscriminatorValue("A")
public class TwoMovingAverages extends Strategy implements Serializable {
	private static final long serialVersionUID = 1L;

	//Length of short average
	private int lengthShort;
	
	//length of long average
	private int lengthLong;
	private double exitThreshold;

	public TwoMovingAverages() {
	}
	
	public TwoMovingAverages(String stock, int size, int lengthShort, 
			int lengthLong, double exitThreshold) {
		super(stock, size);
		this.lengthShort = lengthShort;
		this.lengthLong = lengthLong;
		this.exitThreshold = exitThreshold;
	}

	public double getExitThreshold() {
		return this.exitThreshold;
	}

	public void setExitThreshold(double exitThreshold) {
		this.exitThreshold = exitThreshold;
	}

	public int getLengthLong() {
		return this.lengthLong;
	}

	public void setLengthLong(int lengthLong) {
		this.lengthLong = lengthLong;
	}

	public int getLengthShort() {
		return this.lengthShort;
	}

	public void setLengthShort(int lengthShort) {
		this.lengthShort = lengthShort;
	}

	@Override
	public String toString() {
		return String.format("TwoMovingAverages: [short=%d, long=%d, exit=%1.4f, %s]",
				lengthShort, lengthLong, exitThreshold, stringRepresentation());
	}
}
