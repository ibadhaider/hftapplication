package com.citi.trading.strategy;

import static com.citi.trading.pricing.Pricing.SECONDS_PER_PERIOD;

import java.util.logging.Logger;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.citi.trading.OrderPlacer;
import com.citi.trading.pricing.PriceData;
import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.PricingSource;

/**
 * Implementation of the bollinger-bands trading strategy.
 *
 * @author Will Provost
 */

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BollingerBandsTrader extends Trader<BollingerBands> {

	private static final Logger LOGGER =
	        Logger.getLogger(BollingerBandsTrader.class.getName ());
	    
	    private boolean hitLow = false;
	    private boolean hitHigh = false;
	    
	    public BollingerBandsTrader(PricingSource pricing, 
	    		OrderPlacer market, StrategyPersistence strategyPersistence) {
	    	super(pricing, market, strategyPersistence);
	    }
	    
	    protected int periods(int msec) {
	    	return msec / 1000 / SECONDS_PER_PERIOD;
	    }
	    
	    public int getNumberOfPeriodsToWatch() {
	    	return periods(strategy.getLength());
	    }
	    
	    
	    /**
	     * helper method to update flags: if current price hit stdDev*multiple + (hit high) or - (hit low) avg price
	     * @param data
	     */
	    private void checkAveragesAndDeviation(PriceData data) {
	    	
	        double average = data.getWindowAverage
	    			(periods(strategy.getLength()), PricePoint::getClose);
	        
	        double stdDev = data.getStandardDeviation
	        		(periods(strategy.getLength()), PricePoint::getClose);
	    	
	        double currentPrice = data.getData(1).findAny().get().getClose();
	        
	        if (currentPrice >= average + stdDev*strategy.getMultiple() ) {
	        	hitHigh = true;
	        }else if (currentPrice <= average - stdDev*strategy.getMultiple()  ) {
	        	hitLow = true;
	        }else {
	        	hitHigh = false;
	        	hitLow = false;
	        }
	        
	    	
	    	LOGGER.fine(String.format("Trader " + strategy.getId() + " as of %s average is %1.4f,std dev is %1.4f, current price is %1.4f",
	    			data.getLatestTimestamp(), average, 
	    			stdDev, currentPrice));
	    }
	    
	    /**
	     * When we're open, just check to see if the latest closing price is a profit or
	     * loss greater than our configured threshold. If it is, use the base class'
	     * <strong>Closer</strong> to close the position.
	     */
	    protected void handleDataWhenOpen(PriceData data) { 
	    	
	    	checkAveragesAndDeviation(data);
	    	
	    	double currentPrice = data.getData(1).findAny().get().getClose();
	    	double openingPrice = getStrategy().getOpenPosition().getOpeningTrade().getPrice();
	    	double profitOrLoss = currentPrice / openingPrice - 1.0;
	    	
	    	if (Math.abs(profitOrLoss) > strategy.getExitThreshold()) {
	    		closer.placeOrder(currentPrice);
	    		if (!getStrategy().getOpenPosition().getOpeningTrade().isBuy()) {
	    			profitOrLoss = 0 - profitOrLoss;
	    		}
	    		
	    		LOGGER.info(String.format("Trader " + strategy.getId() + " closing position on a profit/loss of %5.3f percent.", 
	    				profitOrLoss * 100));
	    	}
	    }
	    
	    
	    
	    /**
	     * When we're closed, capture the most recent hit low/high flags,
	     * update to get the new ones, and see if there's been a change.
	     * If hit low, buy to open a long position;
	     * if hit high, sell to open a short position.
	     */
	    protected void handleDataWhenClosed(PriceData data) {
	    	if (tracking.get()) {
	    		checkAveragesAndDeviation(data);
	    		
	    		if (hitLow || hitHigh) {
	    			opener.placeOrder(hitLow, data.getData(1).findAny().get().getClose());
	    			LOGGER.info("Trader " + strategy.getId() + " opening position as it hit " +
	    					(hitLow ? "low" : "high"));
	    		}
		    } else if (data.getSize() >= getNumberOfPeriodsToWatch()) {
		    	checkAveragesAndDeviation(data);
		    	tracking.set(true);
		    	LOGGER.info("Trader " + strategy.getId() + " got initial pricing data and baseline averages and standard deviations.");
		    }
	    }

}
