package com.citi.trading.strategy;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Represents a bollinger bands trading strategy, adding the parameters
 * that are specific to that algorithm.
 * 
 * @author Ibad Haider
 */

@Entity
@DiscriminatorValue("B")
public class BollingerBands extends Strategy implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int length;
	private double multiple;
	private double exitThreshold;

	public BollingerBands() {
	}
	
	public BollingerBands(String stock, int size, int length, 
			double multiple, double exitThreshold) {
		super(stock, size);
		this.length = length;
		this.multiple = multiple;
		this.exitThreshold = exitThreshold;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public double getMultiple() {
		return multiple;
	}

	public void setMultiple(double multiple) {
		this.multiple = multiple;
	}

	public double getExitThreshold() {
		return exitThreshold;
	}

	public void setExitThreshold(double exitThreshold) {
		this.exitThreshold = exitThreshold;
	}
	
	@Override
	public String toString() {
		return String.format("BollingerBands: [length=%d, multiple=%1.4f, exit=%1.4f, %s]",
				length, length, exitThreshold, stringRepresentation());
	}
	
	
}
