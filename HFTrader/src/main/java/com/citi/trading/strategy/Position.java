package com.citi.trading.strategy;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.citi.trading.Trade;
import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * Represents an investment position taken by a strategy.
 * This consists of an opening trade and eventually a closing trade.
 */
@Entity
public class Position implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@OneToOne(optional=false)
	private Trade openingTrade;

    @OneToOne(optional=true)
	private Trade closingTrade;

	@ManyToOne
	@JsonIgnore
	private Strategy strategy;

	public Position() {
	}

	public Position(Strategy strategy, Trade openingTrade) {
		this.strategy = strategy;
		this.openingTrade = openingTrade;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Trade getOpeningTrade() {
		return openingTrade;
	}

	public void setOpeningTrade(Trade openingTrade) {
		this.openingTrade = openingTrade;
	}

	public Trade getClosingTrade() {
		return closingTrade;
	}

	public void setClosingTrade(Trade closingTrade) {
		this.closingTrade = closingTrade;
	}

	public Strategy getStrategy() {
		return this.strategy;
	}

	public void setStrategy(Strategy strategy) {
		this.strategy = strategy;
	}

	public boolean isOpen() {
		return closingTrade == null;
	}
	public boolean isClose() {
		return closingTrade != null;
	}
	
	/**
	 * Reports the profit from a closed trade -- or zero for an open position.
	 */
	public double getProfitOrLoss() {
		if (isOpen()) {
			return 0;
		}
		
		return (closingTrade.getPrice() * closingTrade.getSize() - 
				openingTrade.getPrice() * openingTrade.getSize()) *
				(openingTrade.isBuy() ? 1 : -1);
	}
	
	/**
	 * Report the return on investment, or not-a-number for an open position.
	 */
	public double getROI() {
		return getProfitOrLoss() / openingTrade.getPrice() / openingTrade.getSize();
		
	}
	
	@Override
	public String toString() {
		return String.format("Position: [id=%d, openingTrade=%s, closingTrade=%s]",
				id, openingTrade, closingTrade != null ? closingTrade : "null");
	}
}
