package com.citi.trading.strategy;

import org.springframework.data.repository.CrudRepository;

/**
 * Spring Data repository for {@link Position}s.
 * 
 * @author Will Provost
 */
public interface PositionRepository extends CrudRepository<Position,Integer> {
}
