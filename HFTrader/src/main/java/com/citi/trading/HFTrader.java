package com.citi.trading;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import com.citi.trading.strategy.ActiveTraders;

/**
 * Main application class. Configure the locations of order broker and
 * pricing service in src/main/resources/dev.properties, and run as a 
 * Java or Spring Boot application.
 * 
 * @author Will Provost
 */
@SpringBootApplication
@PropertySource("classpath:dev.properties")
@EnableScheduling
@EnableJms
public class HFTrader {
	
	/**
	 * Provides a REST template for the 
	 * {@link com.citi.trading.pricing.Pricing} client. 
	 */
	@Bean
	public RestTemplate service() {
		return new RestTemplate();
	}
	
	/**
	 * Run the application, letting Spring auto-wiring do most of the work
	 * to assemble the application components. Once everything's in place,
	 * get the {@link ActiveTraders} and start them.
	 */
	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(HFTrader.class, args);
		ActiveTraders activeTraders = context.getBean(ActiveTraders.class);
		activeTraders.start();
	}
}

