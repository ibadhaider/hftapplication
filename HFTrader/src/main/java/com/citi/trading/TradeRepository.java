package com.citi.trading;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

/**
 * Spring Data repository interface for {@link Trade}s.
 * 
 * @author Will Provost
 */
public interface TradeRepository extends CrudRepository<Trade,Integer> {

	/**
	 * Gets all trades since a certain instant.
	 */
	public List<Trade> findByWhenGreaterThanEqual(Timestamp threshold);

	/**
	 * Ease-of-use adapter for {@link #findByWhenGreaterThanEqual(Timestamp)}. 
	 */
	public default List<Trade> findSince(Date threshold) {
		return findByWhenGreaterThanEqual(new Timestamp(threshold.getTime()));
	}
}
