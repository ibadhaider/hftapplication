package com.citi.trading;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Facilitates error reporting for our web services.
 * <ul>
 *   <li>Defines several nested exception classes that can be thrown
 *   by service methods, e.g. {@link #NotFound}.</li>
 *   <li>Defines a JSON-serializable error object.</li>
 *   <li>Serves as a global <strong>@ControllerAdvice</strong> that handles
 *   all if its defined exception types by translating to an HTTP response
 *   with the appropriate status code and the JSON error report as the body.</li>
 *   <li></li>
 * </ul>
 * 
 * @author Will Provost
 */
@ControllerAdvice
public class WebExceptions {

	/**
	 * Transfer object to represent error conditions in XML or JSON formats. Guided
	 * by the JSON API specification for error objects, but only a tiny slice of
	 * that specification.
	 */
	public static class Error {
		public int status;
		public String title;

		/**
		 * Build with an HTTP response code and a message or "title."
		 */
		public Error(int status, String title) {
			this.status = status;
			this.title = title;
		}
	}

	/**
	 * Base class for our custom exceptions. Knows how to translate itself into
	 * an HTTP response with the correct status code and JSON body.
	 */
	public static abstract class WebException extends RuntimeException {

		private HttpStatus status;

		/**
		 * Store off the HTTP status code and the error message.
		 */
		protected WebException(HttpStatus status, String message) {
			super(message);
			this.status = status;
		}

		/**
		 * Build an HTTP response with our status code.
		 * Set the HTTP entity to an object with that same status code and
		 * our error message.
		 */
		public ResponseEntity<Error> produceResponse() {
			HttpHeaders headers = new HttpHeaders();
			headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);

			return new ResponseEntity<>
					(new Error(status.value(), getMessage()), headers, status);
		}
	}

	public static class BadRequest extends WebException {
		public BadRequest(String title) {
			super(HttpStatus.BAD_REQUEST, title);
		}
	}

	public static class Conflict extends WebException {
		public Conflict(String title) {
			super(HttpStatus.CONFLICT, title);
		}
	}

	public static class NotFound extends WebException {
		public NotFound(String title) {
			super(HttpStatus.NOT_FOUND, title);
		}
	}

	/**
	 * "Catch" an excecption of any of our defined types and ask it to
	 * "translate thyself" to an HTTP response. 
	 */
	@ExceptionHandler
	public ResponseEntity<Error> onWebException(WebException ex) {
		return ex.produceResponse();
	}
	
}
