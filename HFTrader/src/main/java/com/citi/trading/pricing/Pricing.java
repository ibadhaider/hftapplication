package com.citi.trading.pricing;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

/**
 * Client component for the pricing service. Holds a publish/subscribe registry
 * so that many subscribers can be notified of pricing data on a given stock,
 * based on a single request to the remote service. When configured as a Spring bean,
 * this component will make HTTP requests on a 15-second timer using 
 * Spring scheduling (if enabled).
 * 
 * Requires a property that provides the URL of the remote service. 
 * 
 * @author Will Provost
 */
@Component
public class Pricing implements PricingSource {

	public static final int MAX_PERIODS_WE_CAN_FETCH = 120;
	public static final int SECONDS_PER_PERIOD = 15;
	
	public static PricePoint parsePricePoint(String CSV) {
		SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String[] fields = CSV.split(",");
		if (fields.length < 6) {
			throw new IllegalArgumentException
				("There must be at least 6 comma-separated fields: '" + CSV + "'");
		}
		
		try {
			Timestamp timestamp = new Timestamp(parser.parse(fields[0]).getTime());
			double open = Double.parseDouble(fields[1]);
			double high = Double.parseDouble(fields[2]);
			double low = Double.parseDouble(fields[3]);
			double close = Double.parseDouble(fields[4]);
			int volume = Integer.parseInt(fields[5]);
			
			return new PricePoint(timestamp, open, high, low, close, volume);
		} catch (Exception ex) {
			throw new RuntimeException("Couldn't parse timestamp.", ex);
		}
	}
	
	@Value("${com.citi.trading.pricing.Pricing.serviceURL}")
	private String serviceURL;
	
    @Autowired
    private PricePointRepository pricePointRepository;
    
    private RestTemplate service;

    @Autowired
    public Pricing(RestTemplateBuilder builder) {
        this.service = builder.build();
    }
    
	private Map<String,PriceData> priceData  = new HashMap<>();
	private Map<String,List<Consumer<PriceData>>> subscribers = new HashMap<>();
	
	/**
	 * If there are no other subscribers for this stock, creates a new 
	 * {@link PriceData} of the requested capacity, registers it for the given
	 * symbol, and registers the subscriber. If there are other subscribers, 
	 * then just adds this subscriber to the list for this symbol, 
	 * and expands the data structure if necessary.
	 */
	public synchronized void subscribe(String symbol, int capacity, Consumer<PriceData> subscriber) {
		
		if (!subscribers.containsKey(symbol)) {
			subscribers.put(symbol, new ArrayList<>());
		}
		subscribers.get(symbol).add(subscriber);
		
		if (!priceData.containsKey(symbol)) {
			priceData.put(symbol, new PriceData(symbol, capacity));
		} else if (priceData.get(symbol).getCapacity() < capacity) {
			priceData.get(symbol).expandTo(capacity);
		}
	}
	
	/**
	 * Removes the subscriber, and if this leaves no other subscribers for
	 * this stock, removes the entry for this stock and lets go of the
	 * associated {@link PriceData}.
	 */
	public synchronized void unsubscribe(String symbol, Consumer<PriceData> subscriber) {
		
		if (subscribers.containsKey(symbol)) {
			if (subscribers.get(symbol).contains(subscriber)) {
				subscribers.get(symbol).remove(subscriber);
				if (subscribers.get(symbol).isEmpty()) {
					subscribers.remove(symbol);
					priceData.remove(symbol);
				}
			} else {
				throw new IllegalStateException("No such subscriber for symbol: " + symbol);
			}
		} else {
			throw new IllegalStateException("No subscribers for symbol: " + symbol);
		}
	}
	
	/**
	 * For each stock for which we have subscribers, requests price data.
	 * If this is our first request for this stock (or first after the data 
	 * structure was expanded) then ask for enough periods to fill the structure,
	 * subject to a cap on number that can be requested imposed by the remote
	 * service. If we already have some data, then ask for the most recent 
	 * price point, and add that to our data.
	 * 
	 * Then save new data and advise each subscriber for that symbol with the new data.
	 */
	@Scheduled(fixedRate=15000)
	@Transactional
	public synchronized void getPriceData() {
		
		for (String symbol : subscribers.keySet()) {
			PriceData prices = priceData.get(symbol);
			int periods = prices.getSize() == 0
					? MAX_PERIODS_WE_CAN_FETCH
					: 1;
			String requestURL = serviceURL + "/" + symbol + "?periods=" + periods;
			String CSV = service.getForObject(requestURL, String.class);
			PricePoint[] pricePoints = Stream.of(CSV.split("\n"))
					.filter(line -> !line.startsWith("timestamp"))
					.map(Pricing::parsePricePoint)
					.peek(p -> p.setStock(symbol))
					.toArray(len -> new PricePoint[len]);
			
			if (prices.addData(pricePoints)) {
				for (Consumer<PriceData> subscriber : subscribers.get(symbol)) {
					subscriber.accept(prices);
				}
			}

			Timestamp latest = pricePointRepository.findLatestTimestamp(symbol);
			pricePointRepository.saveAll(Stream.of(pricePoints)
					.filter(p -> latest == null || p.getTimestamp().after(latest))
					.collect(Collectors.toList()));
			
		}
	}
}
