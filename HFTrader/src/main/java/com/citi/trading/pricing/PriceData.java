package com.citi.trading.pricing;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.ToDoubleFunction;
import java.util.stream.Stream;

/**
 * A data structure that efficiently gathers a list of the N most recent
 * {@link PricePoint}s. Uses a circular-array strategy to store the data,
 * avoiding the higher costs of constantly adding to and removing from an
 * <strong>ArrayList</strong> or other collection.
 * 
 * @author Will Provost
 */
public class PriceData {
	
	private String symbol;
	private PricePoint[] pricePoints;
	private int current = 0;
	private boolean filled = false;
	
	/**
	 * Set up for a specific ticker symbol and with a desired capacity.
	 * The list will never have more than this number of price points at one time.
	 */
	public PriceData(String symbol, int capacity) {
		if (capacity == 0) {
			throw new IllegalArgumentException("Capacity must be a postive number.");
		}
		
		this.symbol = symbol;
		this.pricePoints = new PricePoint[capacity];
	}
	
	/**
	 * Reports the maximum number of data points available at one time.
	 */
	public int getCapacity() {
		return pricePoints.length;
	}
	
	/**
	 * Reports the number of data points currently available. 
	 * May be less than {@link #getCapacity() capacity}. 
	 */
	public int getSize() {
		return filled ? pricePoints.length : current;
	}
	
	public String getSymbol() {
		return symbol;
	}

	/**
	 * Helper method to find the timestamp of the most recent data point.
	 */
	public Timestamp getLatestTimestamp() {
		return getData(getSize()).map(PricePoint::getTimestamp)
				.max(Timestamp::compareTo).get();
	}
	
	/**
	 * Add any number of price points to the array. Initially the array will fill
	 * from the 0 index to its capacity; after that new data points will overwrite 
	 * old ones, effectively in a circle.
	 * 
	 * Data points with timestamps earlier than the latest timestamp found in the
	 * array when the method is called will not be added.
	 * 
	 * @return <strong>true</strong> if any points were added, false if none 
	 */
	public boolean addData(PricePoint... pointsToAdd) {
		
		boolean added = false;
		Optional<Timestamp> latest = getData(getSize())
				.map(PricePoint::getTimestamp).max(Timestamp::compareTo);
		
		for (PricePoint pricePoint : pointsToAdd) {
			if (!latest.isPresent() || pricePoint.getTimestamp().after(latest.get())) {
				pricePoints[current] = pricePoint;
				current = (current + 1) % pricePoints.length;
				if (current == 0) {
					filled = true;
				}
				added = true;
			}
		}
		
		return added;
	}
	
	/**
	 * Increases the capacity of the data structure.
	 * This wipes out any existing data -- necessary because we may well have more
	 * than one dependent checking on the data, and placing the old data in the
	 * new, larger space can't be done in a way that would make all dependents happy.
	 */
	public synchronized void expandTo(int capacity) {
		if (capacity <= pricePoints.length) {
			throw new IllegalArgumentException
				(String.format("Can't expand to %d from %d.", capacity, pricePoints.length));
		}

		current = 0;
		filled = false;
		pricePoints = new PricePoint[capacity];
	}
	
	/**
	 * Gets the most recent N data points, as a stream.
	 * This method accounts for the current position in the circular array,
	 * so that the caller receives a continuous stream of data in 
	 * chronological order. 
	 */
	public synchronized Stream<PricePoint> getData(int numberOfPoints) {
	
		if (numberOfPoints > pricePoints.length) {
			throw new IllegalArgumentException
				(String.format("Can't provide %d data points; configured to hold a max of %d.",
					numberOfPoints, pricePoints.length));
		}
		if (!filled && numberOfPoints > current) {
			throw new IllegalArgumentException
				(String.format("Can't provide %d data points yet; have only filled %d.",
					numberOfPoints, current));
		}
		
		int start = Math.max(0, current - numberOfPoints);
		Stream<PricePoint> segment1 = Arrays.stream(pricePoints, start, current);
		
		start = pricePoints.length - Math.max(0, numberOfPoints - current);
		Stream<PricePoint> segment2 = Arrays.stream(pricePoints, start, pricePoints.length);
		
		return Stream.concat(segment2, segment1);
	}
	
	/**
	 * Calculates a rolling average on the most recent N data points.
	 * 
	 *  @param numberOfPoints Size of the window
	 *  @param field A function that translates a {@link PricePoint} object to
	 *  a number -- which sounds complicated, but for for example you could pass
	 *  <strong>PricePoint::getClose</strong> for a rolling average of the closing price.
	 */
	public double getWindowAverage(int numberOfPoints, ToDoubleFunction<PricePoint> field) {
		if (numberOfPoints == 0) {
			throw new IllegalArgumentException("Can't take an average of no data.");
		}
		
		return getData(numberOfPoints).mapToDouble(field).average().getAsDouble();
	}
	

	
	/**
	 * Calculates the standard deviation on the most recent N data points.
	 * 
	 * @param numberOfPoints
	 * @param field
	 * @return
	 */
	public double getStandardDeviation(int numberOfPoints, ToDoubleFunction<PricePoint> field) {
		double average = getWindowAverage(numberOfPoints, field);
		double stdDev = 0.0;
		
		for (double num : getData(numberOfPoints).mapToDouble(field).toArray()) {
			stdDev += Math.pow(num - average, 2);
		}
		
		return Math.sqrt(stdDev/numberOfPoints);
	}
	

	@Override
	public String toString() {
		return String.format("PriceData: symbol %s, %s data points as of %s", 
				getSymbol(), getSize(), getLatestTimestamp());
	}
}
