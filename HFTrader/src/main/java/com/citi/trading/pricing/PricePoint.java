package com.citi.trading.pricing;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Represents market data for a single instant.
 * 
 * @author Will Provost
 */
@Entity
public class PricePoint {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String stock;
	
	@Column(name="when")
	private Timestamp timestamp;
	
	@Column(name="opn")
	private double open;
	
	private double high;
	private double low;
	
	@Column(name="cls")
	private double close;
	
	private int volume;
	
	public PricePoint(Timestamp timestamp, double open, double high, double low, double close, int volume) {
		super();
		this.timestamp = timestamp;
		this.open = open;
		this.high = high;
		this.low = low;
		this.close = close;
		this.volume = volume;
	}
	
	public PricePoint() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}
	
	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public double getOpen() {
		return open;
	}

	public void setOpen(double open) {
		this.open = open;
	}

	public double getHigh() {
		return high;
	}

	public void setHigh(double high) {
		this.high = high;
	}

	public double getLow() {
		return low;
	}

	public void setLow(double low) {
		this.low = low;
	}

	public double getClose() {
		return close;
	}

	public void setClose(double close) {
		this.close = close;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}
	
	@Override
	public String toString() {
		return String.format("PricePoint: [%s,%1.4f,%1.4f,%1.4f,%1.4f,%d]",
				timestamp, open, high, low, close, volume);
	}
}
