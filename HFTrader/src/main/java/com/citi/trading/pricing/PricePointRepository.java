package com.citi.trading.pricing;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data repository for {@link PricePoint}s.
 * 
 * @author Will Provost
 */
public interface PricePointRepository extends CrudRepository<PricePoint,Integer> {

	/**
	 * Returns the latest timestamp for the given stock.
	 */
	@Query("select max(p.timestamp) from PricePoint p where p.stock = :stock")
	public Timestamp findLatestTimestamp(@Param("stock") String stock); 
	
	/**
	 * Returns all pricing for the given stock between the given times (inclusive),
	 * in chronological order.
	 */
	@Query("select p from PricePoint p where p.stock = :stock and p.timestamp between :start and :end order by p.timestamp")
	public List<PricePoint> findByStockAndRange(@Param("stock") String stock, 
			@Param("start") Timestamp start, @Param("end") Timestamp end);
}
