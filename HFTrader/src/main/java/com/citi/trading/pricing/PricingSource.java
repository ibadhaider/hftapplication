package com.citi.trading.pricing;

import java.util.function.Consumer;

/**
 * An object that registers subscribers and will send them pricing information.
 * 
 * @author Will Provost
 */
public interface PricingSource {
	
	/**
	 * Subscribe to receive pricing updates on this stock.
	 *  
	 * @param symbol Ticker symbol
	 * @param capacity Number of data points you would like to receive, at maximum
	 * @param subscriber Callback function
	 */
	public void subscribe(String symbol, int capacity, Consumer<PriceData> subscriber);
	
	/**
	 * Unsubscribe, cease notifications about this stock.
	 * 
	 * @param symbol Ticker symbol
	 * @param subscriber The consumer you passed when subscribing
	 */
	public void unsubscribe(String symbol, Consumer<PriceData> subscriber);
}
