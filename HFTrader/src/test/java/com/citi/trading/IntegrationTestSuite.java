package com.citi.trading;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * A suite of <em>almost</em> all of the integration tests.
 * Before running this suite, both the PricingService and OrderBroker
 * applications must be running -- and the latter must be in its
 * "alwaysfill" mode to assure predictable results for some components.
 * 
 * We exclude {@link com.citi.trading.strategy.TwoMovingAveragesTraderIntegrationTest}
 * because it takes around 5 minutes to run; just run it separately.
 * 
 * @author Will Provost
 */
@RunWith(Suite.class)
@SuiteClasses({
	com.citi.trading.MarketIntegrationTest.class,
	com.citi.trading.TradeRepositoryIntegrationTest.class,
	com.citi.trading.pricing.PricePointRepositoryIntegrationTest.class,
	com.citi.trading.pricing.PricingIntegrationTest.class,
	com.citi.trading.strategy.StrategyPersistenceIntegrationTest.class,
	com.citi.trading.strategy.StrategyRepositoryIntegrationTest.class
})
public class IntegrationTestSuite {
}
