package com.citi.trading;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Simple integration test for the {@link Market} client:
 * place one trade and watch the console for logging showing the order
 * and the confirmation. Exact results can vary since the mock market
 * itself is not predictable: might reject your order, fill partially, etc.
 * 
 * @author Will Provost
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MarketIntegrationTest {

	@Autowired
	private Market market;
	
	@Test
	public void testPlaceOrder() throws Exception {
		Trade trade = new Trade("AA", true, 1000, 100.0);
		market.placeOrder(trade, System.out::println);
		Thread.sleep(18000); // 15-second delay max for the order to be processed
	}
}

