package com.citi.trading;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * This component is meant for use with unit tests, which use a completely
 * transient, in in-memory database, and with those integration tests that 
 * use the test database. It sets initial test data on startup -- if not found
 * already in place, as it often will be with the test database -- and offers
 * a method to reset to that starter data, suitable for calling from a test's
 * <strong>@Before</strong> method.
 * 
 * @author Will Provost
 */
public class TestDB {

	private static final Logger LOGGER = Logger.getLogger(TestDB.class.getName());
	
	@Autowired
	private DataSource dataSource;
	
    /**
     * Create the test data, if it is not already there (i.e. for in-memory DBs).
     */
	@PostConstruct
    public void init() {
    	
    	final String[] script = {
    		"INSERT INTO trade (when, stock, buy, size, price) VALUES ('2014-07-29 10:00:00', 'IBM', true, 5000, 190)",
    		"INSERT INTO trade (when, stock, buy, size, price) VALUES ('2014-07-29 10:03:00', 'MRK', true, 2500, 55)",
    		"INSERT INTO trade (when, stock, buy, size, price) VALUES ('2014-07-29 10:05:00', 'GE', false, 8000, 25)",
    		"INSERT INTO strategy (ID, strategy_type, stock, size, active, stopping) VALUES (1, 'A', 'MRK', 5000, true, false)",
    		"INSERT INTO two_moving_averages (ID, length_long, length_short, exit_threshold) VALUES (1, 60000, 15000, 0.03)",
    		"INSERT INTO strategy (ID, strategy_type, stock, size, active, stopping) VALUES (2, 'A', 'IBM', 10000, true, false)",
    		"INSERT INTO two_moving_averages (ID, length_long, length_short, exit_threshold) VALUES (2, 60000, 15000, 0.03)",
    		"INSERT INTO position (strategy_ID, opening_trade_ID) VALUES (1, 2)",
    		"INSERT INTO price_point (stock, when, opn, high, low, cls, volume) VALUES ('AA', '2014-07-29 10:05:00', 100, 101, 99, 101, 10000)",
    		"INSERT INTO price_point (stock, when, opn, high, low, cls, volume) VALUES ('AA', '2014-07-29 10:05:15', 101, 102, 100, 102, 10000)",
    		"INSERT INTO price_point (stock, when, opn, high, low, cls, volume) VALUES ('AA', '2014-07-29 10:05:30', 102, 103, 101, 103, 10000)",
    		"INSERT INTO price_point (stock, when, opn, high, low, cls, volume) VALUES ('HON', '2014-07-29 10:05:15', 103, 104, 102, 104, 10000)",
    		"UPDATE id_sequences SET sequence_value=3 WHERE sequence_name='strategies'"
    	};
    			
    	try ( 
    		Connection conn = dataSource.getConnection();
    		Statement stmt = conn.createStatement();
    	) {
    		boolean dataExists = false;
    		try ( ResultSet rs = stmt.executeQuery("SELECT id FROM trade"); ) {
    			dataExists = rs.next();
    		}
    		
    		if (!dataExists) {
	    		for (String update : script) {
	    			stmt.executeUpdate(update);
	    		}
    		}
    	} catch  (Exception ex) {
    		LOGGER.log(Level.WARNING, "Couldn't initialize in-memory database.", ex);
    	}
    }
	
	
    /**
     * Restore the test data.
     */
    public void reset() throws Exception {
    	
    	final String[] script = {
    		"UPDATE position SET closing_trade_id = NULL WHERE id = 1",
    		"UPDATE trade SET size = 2500 WHERE id = 2",
    		"UPDATE strategy set active=true",
    		"DELETE FROM position WHERE id > 1",
    		"DELETE FROM two_moving_averages WHERE id > 2",
    		"DELETE FROM strategy WHERE id > 2",
    		"DELETE FROM trade WHERE id > 3",
    		"DELETE FROM price_point WHERE id > 4",
    		"UPDATE id_sequences SET sequence_value=3 WHERE sequence_name='strategies'"
    	};
    			
    	try ( 
    		Connection conn = dataSource.getConnection();
    		Statement stmt = conn.createStatement();
    	) {
    		for (String update : script) {
    			stmt.executeUpdate(update);
    		}
    	}
    }
}
