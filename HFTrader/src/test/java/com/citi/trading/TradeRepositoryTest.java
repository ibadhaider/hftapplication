package com.citi.trading;

import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Unit tests for the {@link Trade} entity and {@link TradeRepository},
 * using the in-memory database.
 *
 * @author Will Provost
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes=TradeRepositoryTest.Config.class)
public class TradeRepositoryTest extends TradeRepositoryTestBase
{

	@Configuration
	@EnableAutoConfiguration
	@PropertySource("classpath:memoryDB.properties")
	public static class Config {
		
		@Bean
		public TestDB testDB() {
			return new TestDB();
		}
	}
}
