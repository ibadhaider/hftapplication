package com.citi.trading;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	com.citi.trading.MarketTest.class,
	com.citi.trading.TradeRepositoryTest.class,
	com.citi.trading.pricing.PriceDataTest.class,
	com.citi.trading.pricing.PricePointRepositoryTest.class,
	com.citi.trading.pricing.PricingTest.class,
	com.citi.trading.strategy.ActiveTradersTest.class,
	com.citi.trading.strategy.StrategyPersistenceTest.class,
	com.citi.trading.strategy.StrategyRepositoryTest.class,
	com.citi.trading.strategy.TraderServiceMockMvcTest.class,
	com.citi.trading.strategy.TraderServiceTest.class,
	com.citi.trading.strategy.TraderTest.class,
	com.citi.trading.strategy.TwoMovingAveragesTraderTest.class
})
public class UnitTestSuite {
}
