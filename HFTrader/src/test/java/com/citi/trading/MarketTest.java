package com.citi.trading;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.startsWith;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

import java.util.function.Consumer;

import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.support.destination.DestinationResolver;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Unit test for the {@link Market} client component. We configure a mock
 * <strong>JmsTemplate</strong> to disconnect the component from the actual,
 * remote mock market and JMS message broker. We then place an order and 
 * check that the component sends it; and simulate a reply and see that the 
 * component passes that back to our callback method. We also check that the
 * component's timeout feature works correctly. 
 * 
 * @author Will Provost
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes=MarketTest.Config.class)
public class MarketTest {

	@Configuration
	public static class Config {

		@Bean
		public JmsTemplate jmsTemplate() {
			return mock(JmsTemplate.class);
		}

		@Bean
		public Market market() {
			return new Market();
		}
	}

	/**
	 * SImulates a Trade with the following parameters
	 * Buy = true
	 * id = 0
	 * price = 100.0
	 * size = 1000
	 * stock = AA
	 * time = N/A
	 * result = null (b/c no default set)
	 */	
	// Not the whole XML representation -- we leave out the time stamp
	public static final String ORDER_XML = "<trade><buy>true</buy><id>0</id><price>100.0</price><size>1000</size><stock>AA</stock><whenAsDate>";
	
	/**
	 * SImulates a Trade with the following parameters
	 * Buy = true
	 * id = 0
	 * price = 100.0
	 * size = 1000
	 * stock = AA
	 * time =  Date Sta,p
	 * result = filled
	 */	
	// Complete representation, including timestamp and result
	public static final String NOTIFICATION_XML = "<trade><buy>true</buy><id>0</id><price>100.0</price><size>1000</size><stock>AA</stock><whenAsDate>2019-06-02T15:17:04.765-04:00</whenAsDate><result>FILLED</result></trade>";
	
	@Value("${com.citi.trading.Market.orderQueue}")
	private String orderQueue;
	
	@Value("${com.citi.trading.Market.notificationQueue}")
	private String notificationQueue;
	
	@Autowired
	private Market market;
	
	@Autowired
	private JmsTemplate mockTemplate;
	
	@Test
	@DirtiesContext // the mock template shouldn't be shared
	public void testPlaceOrder() throws Exception {

		// Mocks to support sender's query for its reply queue
		Destination mockDestination = mock(Destination.class);
		DestinationResolver mockResolver = mock(DestinationResolver.class);
		when(mockResolver.resolveDestinationName
			(any(Session.class), eq(notificationQueue), eq(false)))
				.thenReturn(mockDestination);
		when(mockTemplate.getDestinationResolver()).thenReturn(mockResolver);
		
		Trade trade = new Trade("AA", true, 1000, 100.0);
		
		@SuppressWarnings("unchecked")
		Consumer<Trade> mockCallback = mock(Consumer.class);
		market.placeOrder(trade, mockCallback);
		
		// Capture the sender's message creator
		ArgumentCaptor<MessageCreator> captor = 
				ArgumentCaptor.forClass(MessageCreator.class);
		verify(mockTemplate).send(eq(orderQueue), captor.capture());
		
		// Invoke the message creator to see that it provides the correct
		// message body, correlation ID, and reply-to queue:
		TextMessage mockOrder = mock(TextMessage.class);
		Session mockSession = mock(Session.class);
		when(mockSession.createTextMessage(argThat(containsString(ORDER_XML))))
		.thenReturn(mockOrder);
		captor.getValue().createMessage(mockSession);
		verify(mockOrder).setJMSReplyTo(mockDestination);
		verify(mockOrder).setJMSCorrelationID(argThat(startsWith("Order")));
		
		// Simulate a notification and confirm that the callback is invoked:
		Message mockNotification = mock(Message.class);
		when(mockNotification.getJMSCorrelationID()).thenReturn("Order1");
		market.onReply(mockNotification, NOTIFICATION_XML);
		
		// Do it again: the callback should by now be un-registered and should not
		// get another call. There will be a warning-level log message instead.
		Message mockNotification2 = mock(Message.class);
		when(mockNotification2.getJMSCorrelationID()).thenReturn("Order1");
		market.onReply(mockNotification2, NOTIFICATION_XML);
		verify(mockCallback).accept(argThat
				(hasProperty("result", equalTo(Trade.Result.FILLED))));
	}

	
	@Test
	@DirtiesContext // the mock template shouldn't be shared
	public void testTimeout() throws Exception {

		// Mocks to support sender's query for its reply queue
		Destination mockDestination = mock(Destination.class);
		DestinationResolver mockResolver = mock(DestinationResolver.class);
		when(mockResolver.resolveDestinationName
			(any(Session.class), eq(notificationQueue), eq(false)))
				.thenReturn(mockDestination);
		when(mockTemplate.getDestinationResolver()).thenReturn(mockResolver);
		
		Trade trade = new Trade("AA", true, 1000, 100.0);
		
		@SuppressWarnings("unchecked")
		Consumer<Trade> mockCallback = mock(Consumer.class);
		market.placeOrder(trade, mockCallback);
		
		// Capture the sender's message creator
		ArgumentCaptor<MessageCreator> captor = 
				ArgumentCaptor.forClass(MessageCreator.class);
		verify(mockTemplate).send(eq(orderQueue), captor.capture());
		
		// Invoke the message creator to see that it provides the correct
		// message body, correlation ID, and reply-to queue:
		TextMessage mockOrder = mock(TextMessage.class);
		Session mockSession = mock(Session.class);
		when(mockSession.createTextMessage(argThat(containsString(ORDER_XML))))
		.thenReturn(mockOrder);
		captor.getValue().createMessage(mockSession);
		verify(mockOrder).setJMSReplyTo(mockDestination);
		verify(mockOrder).setJMSCorrelationID(argThat(startsWith("Order")));
		
		Thread.sleep(Market.TIMEOUT_MSEC + 1000);
		verify(mockCallback).accept(argThat
				(hasProperty("result", equalTo(Trade.Result.REJECTED))));
	}
}
