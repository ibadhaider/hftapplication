package com.citi.trading.pricing;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

import java.util.function.Consumer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Integration test for the {@link Pricing} component. We piggyback on
 * the development configuration, so that we should have connectivity
 * to the remote service, which must be running for this test. Check that
 * we can fetch data and that the publish/subscribe system works.
 * 
 * @author Will Provost
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes=PricingIntegrationTest.Config.class)
public class PricingIntegrationTest {

	@Configuration
	@EnableAutoConfiguration
	@ComponentScan(basePackageClasses=Pricing.class, excludeFilters=
			@ComponentScan.Filter(Configuration.class))
	@PropertySource("classpath:dev.properties")
	public static class Config {
	}
	
	@Autowired
	private Pricing pricing;
	
	/**
	 * There's a very slight chance that our second request for data will go out
	 * at exactly the right moment to get one new price point, so we expect either 
	 * one (likely) or two (unlikely) calls back to our subscriber.
	 * Three is not possible, unless somehow the test case takes 15+ seconds to run.
	 */
	@Test
	public void testSubscribingAndFetching() {
		
		@SuppressWarnings("unchecked")
		Consumer<PriceData> subscriber = (Consumer<PriceData>) mock(Consumer.class);
		
		pricing.subscribe("GOOG", 8, subscriber);
		try {
			pricing.getPriceData();
			pricing.getPriceData();
			pricing.getPriceData();
			verify(subscriber, atMost(2)).accept(argThat(hasProperty("size", equalTo(8))));
		} finally {
			pricing.unsubscribe("GOOG", subscriber);
		}
	}
}

