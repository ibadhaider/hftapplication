package com.citi.trading.pricing;

import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

import java.sql.Timestamp;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.citi.trading.TestDB;

/**
 * Base for testing the {@link PricePoint} entity and {@link PricePointRepository}.
 *
 * @author Will Provost
 */
public abstract class PricePointRepositoryTestBase {
	
	@Autowired
	private TestDB testDB;
	
	@Autowired
	protected PricePointRepository repo;
	
	@Before
	public void resetDatabase() throws Exception {
		testDB.reset();
	}
	
    /**
    Check the count of rows in the test database.
    */
    @Test
    public void testCount () {
        assertThat(repo.count(), equalTo(4L));
    }
    
    /**
    Check for a specific object in the test database.
    */
    @Test
    public void testFind () {
    	PricePoint pricePoint = repo.findById(1).get();
        assertThat(pricePoint.getStock(), equalTo("AA"));
        assertThat(pricePoint.getOpen(), closeTo(100.0, 0.0001));
        assertThat(pricePoint.getHigh(), closeTo(101.0, 0.0001));
        assertThat(pricePoint.getLow(), closeTo(99.0, 0.0001));
        assertThat(pricePoint.getClose(), closeTo(101.0, 0.0001));
        assertThat(pricePoint.getVolume(), equalTo(10000));
    }
    
    /**
     * Check that we can create a price point record, read it back, and remove it.
     */
    @Test
    public void testInsertandRetrieve() {
        long count = repo.count ();
        
    	PricePoint pricePoint = Pricing.parsePricePoint
    			("2019-05-31 13:15:30.620,100.0000,110.0000,90.0000,100.0000,10000");
    	pricePoint.setStock("X");
        repo.save(pricePoint);
        
        PricePoint retrieved = repo.findById(pricePoint.getId()).get();
        assertThat(retrieved.getStock(), equalTo(pricePoint.getStock()));
        assertThat(retrieved.getTimestamp(), equalTo(pricePoint.getTimestamp()));
        assertThat(retrieved.getOpen(), equalTo(pricePoint.getOpen()));
        assertThat(retrieved.getHigh(), equalTo(pricePoint.getHigh()));
        assertThat(retrieved.getLow(), equalTo(pricePoint.getLow()));
        assertThat(retrieved.getClose(), equalTo(pricePoint.getClose()));
        assertThat(retrieved.getVolume(), equalTo(pricePoint.getVolume()));
        
        repo.deleteById(pricePoint.getId ());
        
        assertThat(repo.count(), equalTo(count));
        assertThat(repo.findById(pricePoint.getId()).isPresent(), equalTo(false));
    }
    
    @Test
    public void testFindByStockAndRange() {
    	
    	List<PricePoint> aaPrices = repo.findByStockAndRange
    			("AA", new Timestamp(1306642700000L), new Timestamp(1506642730000L));
    	assertThat(aaPrices, hasSize(3));
    }
}
