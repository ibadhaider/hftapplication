package com.citi.trading.pricing;

import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trading.TestDB;
import com.citi.trading.Trade;

/**
 * Unit tests for the {@link PricePoint} entity and {@link PricePointRepository},
 * using the in-memory database.
 *
 * @author Will Provost
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes=PricePointRepositoryTest.Config.class)
public class PricePointRepositoryTest extends PricePointRepositoryTestBase
{

	@Configuration
	@EnableAutoConfiguration
	@EntityScan(basePackageClasses=Trade.class)
	@PropertySource("classpath:memoryDB.properties")
	public static class Config {
		
		@Bean
		public TestDB testDB() {
			return new TestDB();
		}
	}
}
