package com.citi.trading.pricing;

import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

import java.util.function.Consumer;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.web.client.MockServerRestTemplateCustomizer;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.match.MockRestRequestMatchers;
import org.springframework.test.web.client.response.MockRestResponseCreators;

import com.citi.trading.TestDB;
import com.citi.trading.Trade;
import com.citi.trading.TradeRepository;

/**
 * Unit test for the {@link Pricing} component. We use Spring's built-in
 * mock REST server infrastructure to provide mock responses to pricing 
 * requests. Check that we can fetch data and that the publish/subscribe 
 * system works. There is also a test case for the parsing of CSV price
 * data into {@link PricePoint} objects.
 * 
 * @author Will Provost
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes=PricingTest.Config.class)
public class PricingTest {

	public static final String GOOG_INITIAL_VALUES = 
			"timestamp,open,high,low,close,volume\n" +
			"2019-05-31 17:39:30.620,728.8054,737.1855,717.3219,733.5331,15146\n" + 
			"2019-05-31 17:39:45.620,732.8752,765.5307,719.5634,719.5634,94386\n" + 
			"2019-05-31 17:40:00.620,711.8943,711.8943,680.5442,687.6597,48461\n" + 
			"2019-05-31 17:40:15.620,677.7685,731.8210,677.7685,706.9579,53453\n" + 
			"2019-05-31 17:40:30.620,711.6933,711.6933,696.4495,699.5129,63200\n" + 
			"2019-05-31 17:40:45.620,708.9621,722.1723,702.1713,714.6259,5730\n" + 
			"2019-05-31 17:41:00.620,722.2385,722.2385,703.4747,714.9447,7001\n" + 
			"2019-05-31 17:41:15.620,710.9959,776.9483,702.7601,776.9483,65108";
	public static final String GOOG_NEXT_VALUE1 = 
			"timestamp,open,high,low,close,volume\n" +
			"2019-05-31 17:41:30.620,763.0818,763.0818,723.7756,743.8457,39459";
	public static final String GOOG_NEXT_VALUE2 = 
			"timestamp,open,high,low,close,volume\n" +
			"2019-05-31 17:41:45.620,735.6992,766.9705,734.8760,751.8792,84559";
	
	public static final String AA_JUST_FOUR_VALUES = 
			"timestamp,open,high,low,close,volume\n" +
			"2019-05-31 17:39:30.620,728.8054,737.1855,717.3219,733.5331,15146\n" + 
			"2019-05-31 17:39:45.620,732.8752,765.5307,719.5634,719.5634,94386\n" + 
			"2019-05-31 17:40:00.620,711.8943,711.8943,680.5442,687.6597,48461\n" + 
			"2019-05-31 17:40:15.620,677.7685,731.8210,677.7685,706.9579,53453";
	public static final String AA_INITIAL_VALUES = 
			"timestamp,open,high,low,close,volume\n" +
			"2019-05-31 17:39:30.620,728.8054,737.1855,717.3219,733.5331,15146\n" + 
			"2019-05-31 17:39:45.620,732.8752,765.5307,719.5634,719.5634,94386\n" + 
			"2019-05-31 17:40:00.620,711.8943,711.8943,680.5442,687.6597,48461\n" + 
			"2019-05-31 17:40:15.620,677.7685,731.8210,677.7685,706.9579,53453\n" + 
			"2019-05-31 17:40:30.620,711.6933,711.6933,696.4495,699.5129,63200\n" + 
			"2019-05-31 17:40:45.620,708.9621,722.1723,702.1713,714.6259,5730\n" + 
			"2019-05-31 17:41:00.620,722.2385,722.2385,703.4747,714.9447,7001\n" + 
			"2019-05-31 17:41:15.620,710.9959,776.9483,702.7601,776.9483,65108";

	@Value("${com.citi.trading.pricing.Pricing.serviceURL}")
	private String serviceURL;
	
	@Configuration
	@EnableAutoConfiguration
	@EntityScan(basePackageClasses=Trade.class)
	@EnableJpaRepositories(basePackageClasses=TradeRepository.class)
	@PropertySource("classpath:memoryDB.properties")
	public static class Config {
		
		@Bean
		public TestDB testDB() {
			return new TestDB();
		}
		
		@Bean
		public MockServerRestTemplateCustomizer customizer() {
			return new MockServerRestTemplateCustomizer ();
		}
		
		@Bean
		public Pricing pricing(MockServerRestTemplateCustomizer customizer) {
			return new Pricing(new RestTemplateBuilder(customizer));
		}
	}
	
	@Autowired
	private Pricing pricing;
	
	@Autowired
	private MockServerRestTemplateCustomizer customizer;
	
	@Autowired
	private TestDB testDB;
	
	@Before
	public void setUp() throws Exception {
		testDB.reset();
	}
	
	@Test
	@DirtiesContext
	public void testSubscribingAndFetching() {
		
		MockRestServiceServer mockServer = customizer.getServer();
		mockServer.expect(MockRestRequestMatchers.requestTo(serviceURL + "/GOOG?periods=120")) // was 8 periods
			.andRespond(MockRestResponseCreators.withStatus(HttpStatus.OK)
					.contentType(MediaType.parseMediaType("text/csv")).body(GOOG_INITIAL_VALUES));
		mockServer.expect(MockRestRequestMatchers.requestTo(serviceURL + "/GOOG?periods=1"))
			.andRespond(MockRestResponseCreators.withStatus(HttpStatus.OK)
				.contentType(MediaType.parseMediaType("text/csv")).body(GOOG_NEXT_VALUE1));
		mockServer.expect(MockRestRequestMatchers.requestTo(serviceURL + "/GOOG?periods=1"))
			.andRespond(MockRestResponseCreators.withStatus(HttpStatus.OK)
				.contentType(MediaType.parseMediaType("text/csv")).body(GOOG_NEXT_VALUE2));
		
		@SuppressWarnings("unchecked")
		Consumer<PriceData> subscriber = (Consumer<PriceData>) mock(Consumer.class);
		
		pricing.subscribe("GOOG", 8, subscriber);
		try {
			pricing.getPriceData();
			pricing.getPriceData();
			pricing.getPriceData();
			verify(subscriber, times(3)).accept(argThat(hasProperty("size", equalTo(8))));
		} finally {
			pricing.unsubscribe("GOOG", subscriber);
		}
	}
	
	@Test
	@DirtiesContext
	public void testSubscribingAndFetchingOutOfOrder() {
		
		MockRestServiceServer mockServer = customizer.getServer();
		mockServer.expect(MockRestRequestMatchers.requestTo(serviceURL + "/GOOG?periods=120")) // was 8 periods
			.andRespond(MockRestResponseCreators.withStatus(HttpStatus.OK)
					.contentType(MediaType.parseMediaType("text/csv")).body(GOOG_INITIAL_VALUES));
		mockServer.expect(MockRestRequestMatchers.requestTo(serviceURL + "/GOOG?periods=1"))
			.andRespond(MockRestResponseCreators.withStatus(HttpStatus.OK)
				.contentType(MediaType.parseMediaType("text/csv")).body(GOOG_NEXT_VALUE2));
		mockServer.expect(MockRestRequestMatchers.requestTo(serviceURL + "/GOOG?periods=1"))
			.andRespond(MockRestResponseCreators.withStatus(HttpStatus.OK)
				.contentType(MediaType.parseMediaType("text/csv")).body(GOOG_NEXT_VALUE1));
		
		@SuppressWarnings("unchecked")
		Consumer<PriceData> subscriber = (Consumer<PriceData>) mock(Consumer.class);
		
		pricing.subscribe("GOOG", 8, subscriber);
		try {
			pricing.getPriceData();
			pricing.getPriceData();
			pricing.getPriceData();
			verify(subscriber, times(2)).accept(argThat(hasProperty("size", equalTo(8))));
		} finally {
			pricing.unsubscribe("GOOG", subscriber);
		}
	}
	
	@Test
	@DirtiesContext
	public void testMultipleSubscribers() {
		
		MockRestServiceServer mockServer = customizer.getServer();
		mockServer.expect(MockRestRequestMatchers.requestTo(serviceURL + "/AA?periods=120")) // was 8 periods
			.andRespond(MockRestResponseCreators.withStatus(HttpStatus.OK)
				.contentType(MediaType.parseMediaType("text/csv")).body(AA_INITIAL_VALUES));
		mockServer.expect(MockRestRequestMatchers.requestTo(serviceURL + "/GOOG?periods=120")) // was 8 periods
			.andRespond(MockRestResponseCreators.withStatus(HttpStatus.OK)
				.contentType(MediaType.parseMediaType("text/csv")).body(GOOG_INITIAL_VALUES));
		
		@SuppressWarnings("unchecked")
		Consumer<PriceData> googSubscriber = (Consumer<PriceData>) mock(Consumer.class);
		
		@SuppressWarnings("unchecked")
		Consumer<PriceData> aaSubscriber1 = (Consumer<PriceData>) mock(Consumer.class);
		
		@SuppressWarnings("unchecked")
		Consumer<PriceData> aaSubscriber2 = (Consumer<PriceData>) mock(Consumer.class);
		
		pricing.subscribe("GOOG", 8, googSubscriber);
		pricing.subscribe("AA", 4, aaSubscriber1);
		pricing.subscribe("AA", 8, aaSubscriber2);
		
		try {
			pricing.getPriceData();
			verify(googSubscriber).accept(argThat(hasProperty("size", equalTo(8))));
			verify(aaSubscriber1).accept(argThat(hasProperty("size", equalTo(8))));
			verify(aaSubscriber2).accept(argThat(hasProperty("size", equalTo(8))));
		} finally {
			pricing.unsubscribe("GOOG", googSubscriber);
			pricing.unsubscribe("AA", aaSubscriber1);
			pricing.unsubscribe("AA", aaSubscriber2);
		}
	}
	
	@Test
	@DirtiesContext
	public void testMultipleSubscribersWithExpansion() {
		
		MockRestServiceServer mockServer = customizer.getServer();
		mockServer.expect(MockRestRequestMatchers.requestTo(serviceURL + "/AA?periods=120")) // was 4 periods
			.andRespond(MockRestResponseCreators.withStatus(HttpStatus.OK)
				.contentType(MediaType.parseMediaType("text/csv")).body(AA_JUST_FOUR_VALUES));
		mockServer.expect(MockRestRequestMatchers.requestTo(serviceURL + "/GOOG?periods=120")) // was 8 periods
			.andRespond(MockRestResponseCreators.withStatus(HttpStatus.OK)
				.contentType(MediaType.parseMediaType("text/csv")).body(GOOG_INITIAL_VALUES));
		mockServer.expect(MockRestRequestMatchers.requestTo(serviceURL + "/AA?periods=120")) // was 8 periods
			.andRespond(MockRestResponseCreators.withStatus(HttpStatus.OK)
			.contentType(MediaType.parseMediaType("text/csv")).body(AA_INITIAL_VALUES));
		mockServer.expect(MockRestRequestMatchers.requestTo(serviceURL + "/GOOG?periods=1"))
			.andRespond(MockRestResponseCreators.withStatus(HttpStatus.OK)
			.contentType(MediaType.parseMediaType("text/csv")).body(GOOG_NEXT_VALUE1));
		
		@SuppressWarnings("unchecked")
		Consumer<PriceData> googSubscriber = (Consumer<PriceData>) mock(Consumer.class);
		
		@SuppressWarnings("unchecked")
		Consumer<PriceData> aaSubscriber1 = (Consumer<PriceData>) mock(Consumer.class);
		
		@SuppressWarnings("unchecked")
		Consumer<PriceData> aaSubscriber2 = (Consumer<PriceData>) mock(Consumer.class);
		
		pricing.subscribe("GOOG", 8, googSubscriber);
		pricing.subscribe("AA", 4, aaSubscriber1);
		
		try {
			pricing.getPriceData();
			verify(aaSubscriber1).accept(argThat(hasProperty("size", equalTo(4))));

			pricing.subscribe("AA", 8, aaSubscriber2);
			pricing.getPriceData();
			
			verify(googSubscriber, times(2)).accept(argThat(hasProperty("size", equalTo(8))));
			verify(aaSubscriber1, times(2)).accept(argThat(hasProperty("size", equalTo(8)))); //TODO really?
			verify(aaSubscriber2).accept(argThat(hasProperty("size", equalTo(8))));
		} finally {
			pricing.unsubscribe("GOOG", googSubscriber);
			pricing.unsubscribe("AA", aaSubscriber1);
			pricing.unsubscribe("AA", aaSubscriber2);
		}
	}
	
	@Test
	public void testParsePricePoint() {
		final String CSV = "2019-05-31 13:15:30.620,717.6573,740.3884,717.6573,731.0316,35420";
		PricePoint pricePoint = Pricing.parsePricePoint(CSV);
		
		
		assertThat(pricePoint, notNullValue());
		assertThat(pricePoint.getOpen(), closeTo(717.6573, 0.000001));
		assertThat(pricePoint.getHigh(), closeTo(740.3884, 0.000001));
		assertThat(pricePoint.getLow(), closeTo(717.6573, 0.000001));
		assertThat(pricePoint.getClose(), closeTo(731.0316, 0.000001));
		assertThat(pricePoint.getVolume(), equalTo(35420));
	}
}

