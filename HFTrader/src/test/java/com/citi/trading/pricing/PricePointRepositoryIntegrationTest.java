package com.citi.trading.pricing;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trading.TestDB;
import com.citi.trading.Trade;

/**
 * Integration tests for the {@link PricePoint} entity and {@link PricePointRepository},
 * using the test database.
 *
 * @author Will Provost
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes=PricePointRepositoryIntegrationTest.Config.class)
public class PricePointRepositoryIntegrationTest extends PricePointRepositoryTestBase
{

	@Configuration
	@EnableAutoConfiguration
	@EntityScan(basePackageClasses=Trade.class)
	@PropertySource("classpath:testDB.properties")
	public static class Config {
		
		@Bean
		public TestDB testDB() {
			return new TestDB();
		}
	}

    /**
     * Check that we're prevented from recording a price point without a stock.
     * We do this only on a SQL-scripted database, not the in-memory one because the
     * latter has its metadata generated from the entity classes and there will
     * be no NOT NULL constraint there.
     */
    @Test(expected=DataIntegrityViolationException.class)
    public void testInsertWithoutStock() {
    	PricePoint pricePoint = Pricing.parsePricePoint
    			("2019-05-31 13:15:30.620,100.0000,110.0000,90.0000,100.0000,10000");
        repo.save(pricePoint);
    }
}
