package com.citi.trading.strategy;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isA;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trading.TestDB;
import com.citi.trading.Trade;
import com.citi.trading.WebExceptions;

/**
 * Unit test of the {@link TraderService}, as a POJO.Test cases cover all 
 * methods, including error cases such as no such strader and conflict
 * on creating a trader. We use a mock {@link ActiveTraders} in order to
 * verify back-end operations. 
 * 
 * @author Will Provost
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes=TraderServiceTest.Config.class)
public class TraderServiceTest {

	@Configuration
	@EnableAutoConfiguration
	@EntityScan(basePackageClasses=Trade.class)
	@EnableJpaRepositories(basePackageClasses=Trade.class)
	@PropertySource("classpath:memoryDB.properties")
	public static class Config {
		
		@Bean
		public TestDB testDB() {
			return new TestDB();
		}
		
		@Bean
		public ActiveTraders mockActiveTraders() {
			return mock(ActiveTraders.class);
		}
		
		@Bean
		public TraderService traderService() {
			return new TraderService();
		}
	}
	
	@Autowired
	private TraderService traderService;
	
	@Autowired
	private TestDB testDB;
	
	@Autowired
	private ActiveTraders mockActiveTraders;
	
	@Autowired
	private StrategyRepository strategyRepository;
	
	@Before
	public void setUp() throws Exception {
		testDB.reset();
	}
	
	@Test
	public void testGetTraders() {
		assertThat(traderService.getTraders(), hasSize(2));
	}
	
	@Test
	public void testGetTraderByID() {
		assertThat((TwoMovingAverages) traderService.getTraderById(1), 
				isA(TwoMovingAverages.class));
	}
	
	@Test(expected=WebExceptions.NotFound.class)
	public void testGetTraderByIDNotFound() {
		traderService.getTraderById(55);
	}
	
	@Test
	@DirtiesContext
	public void testDeactivateAndReactivateTrader() {

		traderService.activateTrader(1, "false");
		Strategy strategy = strategyRepository.findById(1).get();
		assertThat(strategy.isActive(), equalTo(false));
		verify(mockActiveTraders).removeTraderFor(any());

		traderService.activateTrader(1, "true");
		strategy = strategyRepository.findById(1).get();
		assertThat(strategy.isActive(), equalTo(true));
		verify(mockActiveTraders).addTraderFor(any());
	}
	
	@Test
	@DirtiesContext
	public void testSaveStrategy() {
		TwoMovingAverages twoMA = 
				new TwoMovingAverages("AA", 100, 30000, 60000, 0.03);
		TwoMovingAverages saved = (TwoMovingAverages) traderService.saveStrategy(twoMA);
		assertThat(saved.getId(), not(equalTo(0)));
		assertThat(strategyRepository.findById(saved.getId()).isPresent(), equalTo(true));
		verify(mockActiveTraders).addTraderFor(any());
	}
	
	@Test(expected=WebExceptions.Conflict.class)
	public void testSaveStrategyConflict() {
		TwoMovingAverages twoMA = 
				new TwoMovingAverages("AA", 100, 30000, 60000, 0.03);
		twoMA.setId(11);
		traderService.saveStrategy(twoMA);
	}
}
