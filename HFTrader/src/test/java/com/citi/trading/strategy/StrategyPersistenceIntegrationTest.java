package com.citi.trading.strategy;

import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trading.TestDB;
import com.citi.trading.Trade;
import com.citi.trading.TradeRepository;

/**
 * Integration tests for the {@link StrategyPersistenceImpl} component,
 * using the test database.
 *
 * @author Will Provost
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes=StrategyPersistenceIntegrationTest.Config.class)
public class StrategyPersistenceIntegrationTest extends StrategyPersistenceTestBase {

	@Configuration
	@EnableAutoConfiguration
	@EntityScan(basePackageClasses=Trade.class)
	@EnableJpaRepositories(basePackageClasses=TradeRepository.class)
	@PropertySource("classpath:testDB.properties")
	public static class Config {
		
		@Bean
		public TestDB testDB() {
			return new TestDB();
		}
		
		@Bean
		public StrategyPersistence strategyPersistence() {
			return new StrategyPersistenceImpl();
		}
	}
}
