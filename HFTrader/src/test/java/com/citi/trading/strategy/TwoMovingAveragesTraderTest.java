package com.citi.trading.strategy;

import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

import java.sql.Timestamp;
import java.util.function.Consumer;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trading.OrderPlacer;
import com.citi.trading.Trade;
import com.citi.trading.pricing.PriceData;
import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.PricingSource;
import com.citi.trading.strategy.TraderTest.TraderMocks;

/**
 * Unit test for the {@link TwoMovingAveragesTrader}. We configure mock
 * pricing, market, and persistence, and aggressively verify outbound calls
 * as a way to check the trading behavior of the component.
 * 
 * @author Will Provost
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes=TwoMovingAveragesTraderTest.Config.class)
@DirtiesContext(classMode=ClassMode.AFTER_EACH_TEST_METHOD)
public class TwoMovingAveragesTraderTest {

	public static final String STOCK = "AA";
	public static final int SIZE = 100;
	public static final double PRICE = 100.0;

	@Configuration
	@Import(TraderMocks.class)
	public static class Config {
		
		@Bean
		public TwoMovingAveragesTrader little2MATrader(PricingSource pricing, 
				OrderPlacer market, StrategyPersistence strategyPersistence) {
			TwoMovingAverages twoMA = 
					new TwoMovingAverages(STOCK,SIZE, 30000, 60000, 0.03);
			TwoMovingAveragesTrader trader = 
					new TwoMovingAveragesTrader(pricing, market, strategyPersistence); 
			trader.setStrategy(twoMA);
			return trader;
		}

		@Bean
		public TwoMovingAveragesTrader bigger2MATrader(PricingSource pricing, 
				OrderPlacer market, StrategyPersistence strategyPersistence) {
			TwoMovingAverages twoMA = 
					new TwoMovingAverages("MRK", SIZE, 60000, 180000, 0.03);
			TwoMovingAveragesTrader trader = 
					new TwoMovingAveragesTrader(pricing, market, strategyPersistence); 
			trader.setStrategy(twoMA);
			return trader;
		}
	}
	
	@Resource(name="little2MATrader")
	private TwoMovingAveragesTrader littleTrader;
	
	@Resource(name="bigger2MATrader")
	private TwoMovingAveragesTrader biggerTrader;
	
	@Autowired
	private OrderPlacer mockMarket;
	
	private Trade latestTrade;
	private Consumer<Trade> latestSubscriber;
	
	/**
	 * Automatically and immediately confirm any requested trade in full.
	 */
	@Before
	@SuppressWarnings("unchecked") // Consumer<Trade> from Object
	public void setUp() {
		doAnswer(inv -> {
				latestTrade = (Trade) inv.getArgument(0);
				latestTrade.setResult(Trade.Result.FILLED);
				latestSubscriber = (Consumer<Trade>) inv.getArgument(1);
				return null;
			}).when(mockMarket).placeOrder(any(), any());		
	}
	
	public static void addPricePoint(PriceData data, double price) {
		Timestamp time = data.getLatestTimestamp();
		PricePoint point = new PricePoint(new Timestamp(time.getTime() + 15000), 
				price, price, price, price, 100);
		data.addData(point);
	}
	
	public static PriceData createPriceData(double... prices) {
		PricePoint[] points = new PricePoint[prices.length];
		Timestamp time = new Timestamp(1546351200000L); // Jan 1 2019, 9:00 AM
		int index = 0;
		for (double price : prices) {
			points[index++] = new PricePoint(time, price, price, price, price, 100);
			time = new Timestamp(time.getTime() + 15000);
		}
		PriceData data = new PriceData("", prices.length);
		data.addData(points);
		return data;
	}
	
	private void confirmAndRecordRequestedTrade(TwoMovingAveragesTrader trader) {
		latestSubscriber.accept(latestTrade);
		if (trader.isOpen()) {
			trader.getStrategy().getOpenPosition().setClosingTrade(latestTrade);
		} else {
			Position position = new Position(trader.getStrategy(), latestTrade);
			trader.getStrategy().addPosition(position);
		}
	}

	@Test
	public void testLongAndCloseAndShort() {
		PriceData data = createPriceData(100, 100, 100, 100);
		littleTrader.accept(data);
		
		addPricePoint(data, 99);
		littleTrader.accept(data);
		verify(mockMarket, never()).placeOrder(any(), any());

		addPricePoint(data, 102);
		littleTrader.accept(data);
		verify(mockMarket).placeOrder((Trade) argThat(both(hasProperty("buy", equalTo(true)))
				.and(hasProperty("price", closeTo(102.0, 0.0001)))), any());
		confirmAndRecordRequestedTrade(littleTrader);
		//openPosition(littleTrader);
		
		addPricePoint(data, 105);
		littleTrader.accept(data);

		addPricePoint(data, 100);
		littleTrader.accept(data);

		addPricePoint(data, 106);
		littleTrader.accept(data);
		verify(mockMarket).placeOrder((Trade) argThat(both(hasProperty("buy", equalTo(false)))
				.and(hasProperty("price", closeTo(106.0, 0.0001)))), any());
		confirmAndRecordRequestedTrade(littleTrader);
		//closePosition(littleTrader);
		
		addPricePoint(data, 107);
		littleTrader.accept(data);
		verify(mockMarket).placeOrder((Trade) argThat(both(hasProperty("buy", equalTo(true)))
				.and(hasProperty("price", closeTo(107.0, 0.0001)))), any());
		
		assertThat(littleTrader.getStrategy().getProfitOrLoss(), closeTo(400.0, 0.0001));
		assertThat(littleTrader.getStrategy().getROI(), closeTo(0.0392, 0.0001));
	}

	@Test
	public void testShort() {
		PriceData data = createPriceData(100, 100, 100, 100);
		littleTrader.accept(data);
		verify(mockMarket, never()).placeOrder(any(), any());
		
		addPricePoint(data, 101);
		littleTrader.accept(data);
		verify(mockMarket, never()).placeOrder(any(), any());

		addPricePoint(data, 98);
		littleTrader.accept(data);
		verify(mockMarket).placeOrder((Trade) argThat(both(hasProperty("buy", equalTo(false)))
				.and(hasProperty("price", closeTo(98.0, 0.0001)))), any());
	}

	/**
	 * Be sure that the trader doesn't jump ahead when initially given 
	 * insufficient data for its long average: it must wait until it has that
	 * much data, and then one more cycle in order to see the averages cross.
	 */
	@Test
	public void testLongButNotTooSoon() {
		PriceData starter = createPriceData(100, 101); 
		PriceData data = new PriceData(STOCK, 4);
		starter.getData(2).forEach(data::addData);
		littleTrader.accept(data);
		
		// Short < long, but too soon ...
		addPricePoint(data, 96);
		littleTrader.accept(data);
		verify(mockMarket, never()).placeOrder(any(), any());

		// Should start tracking now, short > long ...
		addPricePoint(data, 110);
		littleTrader.accept(data);
		verify(mockMarket, never()).placeOrder(any(), any());

		// Aha: short < long and we had enough for a baseline before
		addPricePoint(data, 85);
		littleTrader.accept(data);
		verify(mockMarket).placeOrder((Trade) argThat(both(hasProperty("buy", equalTo(false)))
				.and(hasProperty("price", closeTo(85.0, 0.0001)))), any());
	}

	@Test
	public void testSineWave() {
		PriceData data = createPriceData(63.7505,64.7974,65.6282,66.1616,66.3454,66.1616,65.6282,64.7974,63.7505,62.59,61.4295,60.3826);
		biggerTrader.accept(data);
		
		double toOpenLong[] = { 59.5518,59.0184,58.8346,59.0184,59.5518,60.3826,61.4295,62.59 };
		for (double price : toOpenLong) {
			addPricePoint(data, price);
			biggerTrader.accept(data);
		}
		confirmAndRecordRequestedTrade(biggerTrader);
		
		double toCloseLong[] = { 63.7505,64.7974 };
		for (double price : toCloseLong) {
			addPricePoint(data, price);
			biggerTrader.accept(data);
		}
		confirmAndRecordRequestedTrade(biggerTrader);
		
		double toOpenShort[] = { 65.6282,66.1616,66.3454,66.1616,65.6282,64.7974,63.7505,62.59 };
		for (double price : toOpenShort) {
			addPricePoint(data, price);
			biggerTrader.accept(data);
		}
		confirmAndRecordRequestedTrade(biggerTrader);
		
		double toCloseShort[] = { 61.4295,60.3826 };
		for (double price : toCloseShort) {
			addPricePoint(data, price);
			biggerTrader.accept(data);
		}
		confirmAndRecordRequestedTrade(biggerTrader);
		
		verify(mockMarket).placeOrder((Trade) argThat(both(hasProperty("buy", equalTo(true)))
				.and(hasProperty("price", closeTo(62.59, 0.0001)))), any());
		verify(mockMarket).placeOrder((Trade) argThat(both(hasProperty("buy", equalTo(false)))
				.and(hasProperty("price", closeTo(64.7974, 0.0001)))), any());
		verify(mockMarket).placeOrder((Trade) argThat(both(hasProperty("buy", equalTo(false)))
				.and(hasProperty("price", closeTo(62.59, 0.0001)))), any());
		verify(mockMarket).placeOrder((Trade) argThat(both(hasProperty("buy", equalTo(true)))
				.and(hasProperty("price", closeTo(60.3826, 0.0001)))), any());
		
		assertThat(biggerTrader.getStrategy().getProfitOrLoss(), closeTo(441.48, 0.0001));
		assertThat(biggerTrader.getStrategy().getROI(), closeTo(0.0353, 0.0001));
	}
}
