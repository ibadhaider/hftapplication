package com.citi.trading.strategy;

import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trading.TestDB;
import com.citi.trading.Trade;

/**
 * Integration tests for the {@link Strategy} entity and {@link StrategyRepository},
 * using the test database.
 *
 * @author Will Provost
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes=StrategyRepositoryIntegrationTest.Config.class)
public class StrategyRepositoryIntegrationTest extends StrategyRepositoryTestBase {

	@Configuration
	@EnableAutoConfiguration
	@EntityScan(basePackageClasses=Trade.class)
	@PropertySource("classpath:testDB.properties")
	public static class Config {
		
		@Bean
		public TestDB testDB() {
			return new TestDB();
		}
	}
}
