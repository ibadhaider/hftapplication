package com.citi.trading.strategy;

import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.citi.trading.TestDB;
import com.citi.trading.Trade;
import com.citi.trading.TradeFactory;

/**
 * Test cases for the {@link StrategyPersistenceImpl} component. Test cases
 * cover open, close, and split operations, including confirmation of the
 * verification logic in each operation (e.g. can't close on a stock 
 * that you didn't open).
 * 
 * @author Will Provost
 */
public abstract class StrategyPersistenceTestBase {

	@Autowired
	private TestDB testDB;
	
	@Autowired
	private StrategyRepository strategyRepository;
	
	@Autowired
	private StrategyPersistence service;
	
	@Before
	public void resetDatabase() throws Exception {
		testDB.reset();
	}
	
    @Test
    public void testOpenOnNewStrategy() {
    	TwoMovingAverages strategy = 
    			new TwoMovingAverages("MRK", 100, 60000, 180000, 0.03);
		strategy = strategyRepository.save(strategy);
    			
    	Trade trade = TradeFactory.createTestPurchase();
    	Position position = service.open(strategy, trade);
    	
    	assertThat(position.getOpeningTrade().getStock(), equalTo(trade.getStock()));
    	assertThat(position.getStrategy().getId(), equalTo(strategy.getId()));
    	assertThat(position.isOpen(), equalTo(true));
    }
	
    @Test
    public void testOpen() {
    	Strategy strategy = strategyRepository.findStrategyAndPositions(2);
    	strategy.setActive(false); // plant of stale data
    	Trade trade = TradeFactory.createTestPurchase();
    	Position position = service.open(strategy, trade);
    	
    	assertThat(position.getOpeningTrade().getStock(), equalTo(trade.getStock()));
    	assertThat(position.getStrategy().getId(), equalTo(strategy.getId()));
    	assertThat(position.isOpen(), equalTo(true));
    	
    	Strategy updatedStrategy = strategyRepository.findStrategyAndPositions(2);
    	Position updatedPosition = updatedStrategy.getPositions().get(0);
    	assertThat(updatedPosition.getOpeningTrade().getStock(), equalTo(trade.getStock()));
    	assertThat(updatedPosition.getStrategy().getId(), equalTo(strategy.getId()));
    	assertThat(updatedPosition.isOpen(), equalTo(true));

    	// Don't take the caller's word for the starting state of the strategy:
    	// should query for it, edit it, and save
    	assertThat(updatedStrategy.isActive(), equalTo(true));
    }
	
    @Test
    public void testClose() {
    	Strategy strategy = strategyRepository.findStrategyAndPositions(1);
    	strategy.setActive(false); // plant of stale data
    	Trade trade = new Trade("MRK", false, 2500, 56.0);
    	Position position = service.close(strategy, trade);
    	
    	assertThat(position.getClosingTrade().getStock(), equalTo(trade.getStock()));
    	assertThat(position.getStrategy().getId(), equalTo(strategy.getId()));
    	assertThat(position.isOpen(), equalTo(false));
    	assertThat(position.getProfitOrLoss(), closeTo(2500.0, 0.0001));
    	
    	Strategy updatedStrategy = strategyRepository.findStrategyAndPositions(1);
    	Position updatedPosition = updatedStrategy.getPositions().get(0);
    	assertThat(updatedPosition.getClosingTrade().getStock(), equalTo(trade.getStock()));
    	assertThat(updatedPosition.getStrategy().getId(), equalTo(strategy.getId()));
    	assertThat(updatedPosition.isOpen(), equalTo(false));
    	assertThat(updatedPosition.getProfitOrLoss(), closeTo(2500.0, 0.0001));
    	
    	// Don't take the caller's word for the starting state of the strategy:
    	// should query for it, edit it, and save
    	assertThat(updatedStrategy.isActive(), equalTo(true));
    }
	
    @Test(expected=IllegalStateException.class)
    public void testCloseBeforeOpen() {
    	Strategy strategy = strategyRepository.findStrategyAndPositions(2);
    	Trade trade = new Trade("MRK", false, 2500, 56.0);
    	service.close(strategy, trade);
    }
	
    @Test(expected=IllegalStateException.class)
    public void testOpenAfterOpen() {
    	Strategy strategy = strategyRepository.findStrategyAndPositions(1);
    	Trade trade = new Trade("MRK", false, 2500, 56.0);
    	service.open(strategy,  trade);
    }
	
    @Test(expected=IllegalArgumentException.class)
    public void testCloseMismatchedStock() {
    	Strategy strategy = strategyRepository.findStrategyAndPositions(1);
    	Trade trade = new Trade("MSFT", false, 2500, 56.0);
    	service.close(strategy, trade);
    }
	
    @Test(expected=IllegalArgumentException.class)
    public void testCloseMismatchedBuySell() {
    	Strategy strategy = strategyRepository.findStrategyAndPositions(1);
    	Trade trade = new Trade("MRK", true, 2500, 56.0);
    	service.close(strategy, trade);
    }
	
    @Test(expected=IllegalArgumentException.class)
    public void testCloseMismatchedSize() {
    	Strategy strategy = strategyRepository.findStrategyAndPositions(1);
    	Trade trade = new Trade("MRK", false, 2400, 56.0);
    	service.close(strategy, trade);
    }
	
    @Test
    public void testSplit() {
    	
    	Strategy strategy = strategyRepository.findStrategyAndPositions(1);
    	strategy.setActive(false); // plant of stale data
    	Position originalPosition = strategy.getOpenPosition();
    	Trade trade = new Trade("MRK", false, 2000, 56.0);
    	Position newPosition = service.splitAndClosePart(strategy, trade);

    	assertThat(originalPosition.getOpeningTrade().getSize(), equalTo(2000));
    	assertThat(newPosition.getOpeningTrade().getSize(), equalTo(500));
    	assertThat(newPosition.getStrategy().getId(), equalTo(strategy.getId()));
    	assertThat(newPosition.isOpen(), equalTo(true));
    	
    	Strategy updatedStrategy = strategyRepository.findStrategyAndPositions(1);
    	Position firstPosition = updatedStrategy.getPositions().get(0);
    	assertThat(firstPosition.getClosingTrade().getStock(), equalTo(trade.getStock()));
    	assertThat(firstPosition.getStrategy().getId(), equalTo(strategy.getId()));
    	assertThat(firstPosition.isOpen(), equalTo(false));
    	Position secondPosition = updatedStrategy.getPositions().get(1);
    	assertThat(secondPosition.getOpeningTrade().getStock(), equalTo(trade.getStock()));
    	assertThat(secondPosition.getStrategy().getId(), equalTo(strategy.getId()));
    	assertThat(secondPosition.isOpen(), equalTo(true));
    	
    	// Don't take the caller's word for the starting state of the strategy:
    	// should query for it, edit it, and save
    	assertThat(updatedStrategy.isActive(), equalTo(true));
    }

    @Test(expected=IllegalArgumentException.class)
    public void testSplitZero() {
    	Strategy strategy = strategyRepository.findStrategyAndPositions(1);
    	Trade trade = new Trade("MRK", false, 0, 56.0);
    	service.splitAndClosePart(strategy, trade);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testSplitAll() {
    	Strategy strategy = strategyRepository.findStrategyAndPositions(1);
    	Trade trade = new Trade("MRK", false, 2500, 56.0);
    	service.splitAndClosePart(strategy, trade);
    }
    
    /**
     * set the trading start time, set the stop time, set a new start time,
     * and check that the timestamps are managed correctly.
     */
    @Test
    public void testStartAndStopAndStart() {
    	Strategy strategy = strategyRepository.findById(1).get();

    	strategy = service.startTrading(strategy);
    	assertThat(strategy.getStartedTrading(), notNullValue());
    	assertThat(strategy.getStoppedTrading(), nullValue());
    	strategy = strategyRepository.findById(1).get();
    	assertThat(strategy.getStartedTrading(), notNullValue());
    	assertThat(strategy.getStoppedTrading(), nullValue());
    	
    	strategy = service.stopTrading(strategy);
    	assertThat(strategy.getStartedTrading(), notNullValue());
    	assertThat(strategy.getStoppedTrading(), notNullValue());
    	strategy = strategyRepository.findById(1).get();
    	assertThat(strategy.getStartedTrading(), notNullValue());
    	assertThat(strategy.getStoppedTrading(), notNullValue());

    	strategy = service.startTrading(strategy);
    	assertThat(strategy.getStartedTrading(), notNullValue());
    	assertThat(strategy.getStoppedTrading(), nullValue());
    	strategy = strategyRepository.findById(1).get();
    	assertThat(strategy.getStartedTrading(), notNullValue());
    	assertThat(strategy.getStoppedTrading(), nullValue());
    }
}
