package com.citi.trading.strategy;

import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.citi.trading.TestDB;
import com.citi.trading.Trade;

/**
 * Test cases for the {@link Strategy} entity and {@link StrategyRepository}.
 *
 * @author Will Provost
 */
public abstract class StrategyRepositoryTestBase {
	
	@Autowired
	private TestDB testDB;
	
	@Autowired
	private StrategyRepository repo;
	
	@Before
	public void resetDatabase() throws Exception {
		testDB.reset();
	}

    /**
    Check the count of rows in the test database.
    */
    @Test
    public void testCount() {
        assertThat(repo.count(), equalTo(2L));
    }
    
    /**
    Check for a specific object in the test database.
    */
    @Test
    public void testFind () {
        Strategy strategy = repo.findById(1).get();
        assertThat(strategy.getStock(), equalTo("MRK"));
        assertThat(strategy.getSize(), equalTo(5000));
        assertThat(strategy.isActive(), equalTo(true));
        
        assertThat(strategy instanceof TwoMovingAverages, equalTo(true));
        TwoMovingAverages moving = (TwoMovingAverages) strategy;
        assertThat(moving.getLengthLong(), equalTo(60000));
        assertThat(moving.getLengthShort(), equalTo(15000));
        assertThat(moving.getExitThreshold(), closeTo(0.03, 0.000001));
    }
    
    /**
    Check that we can find the trades attributed to a given strategy
    in the test database.
    */
    @Test
    public void testFindStrategyAndPositions() {
        Strategy strategy = repo.findStrategyAndPositions(1);
        assertThat(strategy.getStock(), equalTo("MRK"));
        assertThat(strategy.getSize(), equalTo(5000));
        assertThat(strategy.isActive(), equalTo(true));
        
        assertThat(strategy instanceof TwoMovingAverages, equalTo(true));
        TwoMovingAverages moving = (TwoMovingAverages) strategy;
        assertThat(moving.getLengthLong(), equalTo(60000));
        assertThat(moving.getLengthShort(), equalTo(15000));
        assertThat(moving.getExitThreshold(), closeTo(0.03, 0.000001));

        List<Position> positions = strategy.getPositions();
        assertThat(positions, hasSize(1));
        Trade trade = positions.get(0).getOpeningTrade();
        assertThat(trade.getId(), equalTo(2));
        assertThat(positions.get(0).getClosingTrade(), nullValue());
        assertThat(positions.get(0).isOpen(), equalTo(true));
    }
    
    /**
    Check that we can find a given strategy and confirm that it has yet to trade.
    */
    @Test
    public void testFindStrategyAndNoPositions() {
        Strategy strategy = repo.findStrategyAndPositions(2);
        List<Position> positions = strategy.getPositions();
        assertThat(positions, hasSize(0));
    }
    
    /**
    Test that we can create a new {@link TwoMovingAverages} row.
    */
    @Test
    public void testCreateStrategy () {
        TwoMovingAverages strategy = 
        		new TwoMovingAverages ("GG", 6800, 30000, 30000, 0.03);
        Strategy persistent = repo.save(strategy);
        repo.deleteById(persistent.getId ());
    }
    
    /**
    Check the count of active strategies in the test database. 
    */
    @Test
    public void testGetActiveStrategies () {
        List<Strategy> activeStrategies = repo.findActiveStrategies();
        assertThat(activeStrategies, hasSize(2));
    }
}
