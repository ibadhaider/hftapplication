package com.citi.trading;

import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Base for testing the {@link Trade} entity and {@link TradeRepository}.
 *
 * @author Will Provost
 */
public abstract class TradeRepositoryTestBase
{
	
	@Autowired
	private TestDB testDB;
	
	@Autowired
	private TradeRepository repo;
	
	@Before
	public void resetDatabase() throws Exception {
		testDB.reset();
	}
	
    /**
    Check the count of rows in the test database.
    */
    @Test
    public void testCount ()
        throws Exception
    {
        assertThat(repo.count(), equalTo(3L));
    }
    
    /**
    Check for a specific object in the test database.
    */
    @Test
    public void testFind ()
        throws Exception
    {
        Trade trade = repo.findById(1).get();
        assertThat(trade.getStock(), equalTo("IBM"));
        assertThat(trade.isBuy (), equalTo(true));
        assertThat(trade.getSize (), equalTo(5000));
        assertThat(trade.getPrice (), closeTo(190, 0.0001));
    }
    
    /**
    Check that we can create a trade record, read it back, and remove it.
    */
    @Test
    public void testInsertRetrieveAndDelete ()
        throws Exception
    {
        long count = repo.count ();
        
        Trade trade = TradeFactory.createTestSale ();
        repo.save(trade);
        
        Trade retrieved = repo.findById(trade.getId()).get();
        assertThat(retrieved.getWhen(), equalTo(trade.getWhen()));
        assertThat(retrieved, TradeFactory.isTestSale());
        
        repo.deleteById(trade.getId ());
        
        assertThat(repo.count(), equalTo(count));
        assertThat(repo.findById(trade.getId ()).isPresent(), equalTo(false));
    }
    
    /**
    Check that we can get a set of trades since a given date.
    */
    @Test
    public void testGetTradesSince ()
        throws Exception
    {
        Calendar calendar = Calendar.getInstance ();
        calendar.set (Calendar.YEAR, 2014);
        calendar.set (Calendar.MONTH, 6); // July
        calendar.set (Calendar.DAY_OF_MONTH, 29);
        calendar.set (Calendar.HOUR_OF_DAY, 10);
        calendar.set (Calendar.MINUTE, 2);
        calendar.set (Calendar.SECOND, 0);

        assertThat(repo.findSince(calendar.getTime ()), hasSize(2));
    }
}
