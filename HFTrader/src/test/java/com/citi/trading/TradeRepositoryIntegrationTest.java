package com.citi.trading;

import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Integration tests for the {@link Trade} entity and {@link TradeRepository},
 * using the test database.
 *
 * @author Will Provost
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes=TradeRepositoryIntegrationTest.Config.class)
public class TradeRepositoryIntegrationTest extends TradeRepositoryTestBase
{

	@Configuration
	@EnableAutoConfiguration
	@PropertySource("classpath:testDB.properties")
	public static class Config {
		
		@Bean
		public TestDB testDB() {
			return new TestDB();
		}
	}
}
