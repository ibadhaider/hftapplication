package com.citi.trading;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;

import java.sql.Timestamp;

import org.hamcrest.Matcher;

/**
 * Utility for basic persistence testing.
 *
 * @author Will Provost
 */
public class TradeFactory
{
    public static final String BUY_STOCK = "HON";
    public static final int BUY_SIZE = 2000;
    public static final double BUY_PRICE = 88;

    public static final String SALE_STOCK = "GE";
    public static final int SALE_SIZE = 1000;
    public static final double SALE_PRICE = 100;

    /**
    Factory for a simple trade object representing a purchase.
    */
    public static Trade createTestPurchase ()
    {
        Trade trade = new Trade ();
        trade.setWhen (new Timestamp (System.currentTimeMillis ()));
        trade.setStock (BUY_STOCK);
        trade.setBuy (true);
        trade.setSize (BUY_SIZE);
        trade.setPrice (BUY_PRICE);
        return trade;
    }
    
    /**
    Helper to assert that a given object has the same values as
    our {@link #createTestPurchase standard purchase}.
    */
    public static Matcher<Trade> isTestPurchase() {
    	return allOf
			(hasProperty("stock", equalTo(BUY_STOCK)),
			 hasProperty("buy", equalTo(true)),
			 hasProperty("size", equalTo(BUY_SIZE)),
			 hasProperty("price", closeTo(BUY_PRICE, 0.0001)));
    }

    /**
    Factory for a simple trade object representing a sale.
    */
    public static Trade createTestSale ()
    {
        Trade trade = new Trade ();
        trade.setWhen (new Timestamp (System.currentTimeMillis ()));
        trade.setStock (SALE_STOCK);
        trade.setBuy (false);
        trade.setSize (SALE_SIZE);
        trade.setPrice (SALE_PRICE);
        return trade;
    }
    
    /**
    Helper to assert that a given object has the same values as
    our {@link #createTestSale standard sale}.
    */
    public static Matcher<Trade> isTestSale () {
    	return allOf
			(hasProperty("stock", equalTo(SALE_STOCK)),
			 hasProperty("buy", equalTo(false)),
			 hasProperty("size", equalTo(SALE_SIZE)),
			 hasProperty("price", closeTo(SALE_PRICE, 0.0001)));
    }
}
