SET SCHEMA Trades;

ALTER TABLE position DROP CONSTRAINT position_strategy;
ALTER TABLE position DROP CONSTRAINT position_trade_1;
ALTER TABLE position DROP CONSTRAINT position_trade_2;
ALTER TABLE two_moving_averages DROP CONSTRAINT two_moving_averages_strategy;
ALTER TABLE bollinger_bands DROP CONSTRAINT bollinger_bands_strategy;


DROP TABLE price_point;
DROP TABLE position;
DROP TABLE two_moving_averages;
DROP TABLE bollinger_bands;
DROP TABLE strategy;
DROP TABLE id_sequences;
DROP TABLE trade;

DROP SCHEMA Trades RESTRICT;
